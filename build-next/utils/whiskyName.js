'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = whiskyName;
const BOTLLERS = {
	'anam na h-alba': 'AnhA',
	'càrn mòr': 'CM',
	'creative whisky company': 'CWC',
	'gordon & macphail': 'GM',
	'single & single': 'S&S',
	'jack wiebers whisky world': 'JW',
	'jewish whisky company': 'JWC',
	'whiskybroker.co.uk': 'WhB',
	'whiskykanzler': 'Wk',
	'whiskykeller': 'Whk'
};

function whiskyName(whisky) {
	let subtitle = '';
	let titlePart1 = '';
	let titlePart2 = '';
	let titlePart3 = '';
	let distilleries = whisky.distillery ? whisky.distillery.split(/(\s*,\s*)|(\s&\s)/) : [];

	if (whisky.age) {
		titlePart2 += `${ whisky.age }\xa0yo`;
	}

	if (!whisky.name && whisky.vintage && whisky.vintage.split('-').length >= 3) {
		let date = whisky.vintage.match(/^\d{4}/);

		titlePart2 += ` ${ date[0] }`;
	}

	if (whisky.name) {
		titlePart2 += ` ${ whisky.name }`;
	}

	if (whisky.bottler && whisky.distillery) {
		if (BOTLLERS[whisky.bottler.toLowerCase()]) {
			titlePart3 = `(${ BOTLLERS[whisky.bottler.toLowerCase()] })`;
		} else {
			let parts = whisky.bottler.split(/\W+/).filter(part => !!part);

			if (parts.length === 1) {
				titlePart3 = `(${ parts[0][0] }${ parts[0][1] ? parts[0][1] : '' })`.toUpperCase();
			} else if (parts.length > 1) {
				titlePart3 = `(${ parts.map(part => {
					if (part.toLowerCase() === 'limited' || part.toLowerCase() === 'ltd' || part.toLowerCase() === 'company' || part.toLowerCase() === 'co') {
						return '';
					}

					if (part.toLowerCase().startsWith('mac')) {
						return 'Mc';
					}

					return part[0].toUpperCase();
				}).join('') })`;
			}
		}
	}

	if (whisky.brand) {
		titlePart1 = whisky.brand;
	} else if (distilleries.length === 1) {
		titlePart1 = distilleries[0];
	} else if (whisky.bottler && (!whisky.name || whisky.name && whisky.age)) {
		titlePart1 = whisky.bottler;
		titlePart3 = '';
	}

	if (whisky.edition) {
		subtitle += whisky.edition;
	}

	if (whisky.batch) {
		subtitle += ` ${ whisky.batch }`;
	}

	if (!titlePart2) {
		if (!titlePart2 && whisky.vintage) {
			let date = whisky.vintage.match(/^\d{4}/);

			titlePart2 = date[0];
		} else {
			titlePart2 = subtitle;
			subtitle = '';
		}
	}

	if (titlePart1 && titlePart2) {
		titlePart1 = `${ titlePart1.trim() } `;
	}

	if (titlePart3 && titlePart2) {
		titlePart3 = ` ${ titlePart3.trim() }`;
	}

	if (titlePart2 && !titlePart3 && subtitle) {
		titlePart2 = `${ titlePart2 } `;
	}

	if (titlePart3 && subtitle) {
		titlePart3 = `${ titlePart3 } `;
	}

	return [titlePart1, titlePart2, titlePart3, subtitle];
}
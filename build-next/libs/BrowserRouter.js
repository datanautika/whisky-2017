'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// regex for stripping a leading hash/slash and trailing space.
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

// regex for stripping leading and trailing slashes.
const ROOT_STRIPPER = /^\/+|\/+$/g;

// regex for stripping urls of hash.
const PATH_STRIPPER = /#.*$/;

/**
 * Normalizes path fragment by stripping a leading hash/slash and trailing space.
 *
 * @param {string} fragment
 * @returns {string}
 */
function normalizePathFragment(fragment) {
	return fragment.replace(ROUTE_STRIPPER, '');
}

let browserRouter;

/**
 * BrowserRouter class.
 * Singleton.
 */

let BrowserRouter = function () {

	/**
  * Creates a browserRouter instance.
  *
  * @returns {BrowserRouter}
  */
	function BrowserRouter(router) {
		_classCallCheck(this, BrowserRouter);

		this.location = global.location;
		this.history = global.history;
		this.fragment = '';
		this.router = null;
		this.root = '/';
		this.isSilent = false;
		this.isStarted = false;

		browserRouter = browserRouter ? browserRouter : this;

		this.router = router;

		return browserRouter;
	}

	/**
  * Are we at the app root?
  */


	_createClass(BrowserRouter, [{
		key: 'start',


		/**
   * Starts the router.
   * @param {string} options.root
   * @param {boolean} options.isSilent
   * @returns {boolean}
   */
		value: function start() {
			var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
			    _ref$root = _ref.root;

			let root = _ref$root === undefined ? '/' : _ref$root;
			var _ref$isSilent = _ref.isSilent;
			let isSilent = _ref$isSilent === undefined ? false : _ref$isSilent;

			this.root = root;
			this.isSilent = isSilent;
			this.fragment = this.path;

			// Normalize root to always include a leading and trailing slash.
			this.root = `/${ this.root }/`.replace(ROOT_STRIPPER, '/');

			(0, _jquery2.default)(document).on('scroll', _lodash2.default.debounce(() => {
				this.history.replaceState({
					scroll: (0, _jquery2.default)(document).scrollTop()
				}, document.title);
			}, 40));

			window.addEventListener('popstate', event => {
				let current = this.path;

				if (current === this.fragment) {
					return false;
				}

				this.router.trigger(current);

				this.fragment = current;

				return true;
			});

			this.history.scrollRestoration = 'manual';
			this.isStarted = true;

			if (!this.isSilent) {
				return this.router.trigger(this.fragment);
			}

			return false;
		}

		/**
   * Stops the router.
   *
   * @returns {boolean}
   */

	}, {
		key: 'stop',
		value: function stop() {
			// Remove window listeners
			window.removeEventListener('popstate');

			this.history.scrollRestoration = 'manual';
			this.isStarted = false;

			return false;
		}

		/**
   * Updates the URL.
   * Works iff router is started and the script is running in browser.
   *
   * @param {string} fragment
   * @param {boolean} options.trigger
   * @param {boolean} options.replace
   * @returns {boolean}
   */

	}, {
		key: 'navigate',
		value: function navigate() {
			let fragment = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

			var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
			    _ref2$trigger = _ref2.trigger;

			let trigger = _ref2$trigger === undefined ? true : _ref2$trigger;
			var _ref2$replace = _ref2.replace;
			let replace = _ref2$replace === undefined ? false : _ref2$replace;
			var _ref2$resetScrollPosi = _ref2.resetScrollPosition;
			let resetScrollPosition = _ref2$resetScrollPosi === undefined ? true : _ref2$resetScrollPosi;

			if (!this.isStarted) {
				return false;
			}

			let newFragment = normalizePathFragment(fragment);
			let url = this.root + newFragment;

			// Strip the hash and decode for matching.
			newFragment = decodeURI(newFragment.replace(PATH_STRIPPER, ''));

			if (this.fragment === newFragment) {
				return false;
			}

			this.fragment = newFragment;

			// Don't include a trailing slash on the root.
			if (this.fragment === '' && url !== '/') {
				url = url.slice(0, -1);
			}

			// set the fragment as a real URL.
			if (resetScrollPosition) {
				this.history.replaceState({ scroll: (0, _jquery2.default)(document).scrollTop() }, window.document.title);
				this.history[replace ? 'replaceState' : 'pushState']({ scroll: 0 }, window.document.title, url);
			} else {
				this.history.replaceState({ scroll: (0, _jquery2.default)(document).scrollTop() }, window.document.title);
				this.history[replace ? 'replaceState' : 'pushState']({}, window.document.title, url);
			}

			if (trigger) {
				return this.router.trigger(this.fragment);
			}

			return false;
		}
	}, {
		key: 'isAtRoot',
		get: function get() {
			return this.location && this.location.pathname.replace(/[^\/]$/, '$&/') === this.root && !this.search;
		}

		/**
   * In IE6, the hash fragment and search params are incorrect if the fragment contains `?`
   */

	}, {
		key: 'search',
		get: function get() {
			let match = this.location.href.replace(/#.*/, '').match(/\?.+/);

			return match ? match[0] : '';
		}

		/**
   * Get the pathname and search params, without the root.
   */

	}, {
		key: 'path',
		get: function get() {
			let path = decodeURI(this.location.pathname + this.search);
			let root = this.root.slice(0, -1);

			if (!path.indexOf(root)) {
				path = path.slice(root.length);
			}

			return path.slice(1).replace(ROUTE_STRIPPER, '');
		}
	}]);

	return BrowserRouter;
}();

exports.default = BrowserRouter;
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Category = require('./Category.css');

var _Category2 = _interopRequireDefault(_Category);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _data = require('../data');

var _whiskyName = require('../utils/whiskyName');

var _whiskyName2 = _interopRequireDefault(_whiskyName);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

let Category = function (_React$Component) {
	_inherits(Category, _React$Component);

	function Category() {
		_classCallCheck(this, Category);

		return _possibleConstructorReturn(this, (Category.__proto__ || Object.getPrototypeOf(Category)).apply(this, arguments));
	}

	_createClass(Category, [{
		key: 'render',
		value: function render() {
			let category = this.props.data;
			let categoryResults = _lodash2.default.find(_data.votesByCategory.values, { groups: { categoryName: category.name } });
			let selectedWhiskies = [];
			let whiskiesElements;

			if (categoryResults) {
				categoryResults.whiskyIds.forEach((whiskyId, index) => {
					let whisky = _lodash2.default.find(selectedWhiskies, { whisky: { id: whiskyId } });

					if (whisky) {
						whisky.pointsCount += _lodash2.default.find(_data.votes, { id: categoryResults.voteIds[index] }).pointsCount;
					} else {
						selectedWhiskies.push({
							whisky: _lodash2.default.find(_data.whiskies, { id: whiskyId }),
							pointsCount: _lodash2.default.find(_data.votes, { id: categoryResults.voteIds[index] }).pointsCount
						});
					}
				});

				whiskiesElements = selectedWhiskies.sort((a, b) => {
					if (category.id === 14) {
						// "Zklamání/propadák roku"
						let result = a.pointsCount - b.pointsCount;

						if (result !== 0) {
							return result;
						}

						return (0, _whiskyName2.default)(a.whisky).join(' ').trim().localeCompare((0, _whiskyName2.default)(b.whisky).join(' ').trim());
					}

					let result = b.pointsCount - a.pointsCount;

					if (result !== 0) {
						return result;
					}

					return (0, _whiskyName2.default)(a.whisky).join(' ').trim().localeCompare((0, _whiskyName2.default)(b.whisky).join(' ').trim());
				}).map((selectedWhisky, index) => _react2.default.createElement(
					'li',
					{ key: index, className: _Category2.default.whisky },
					_react2.default.createElement(_WhiskyName2.default, { data: selectedWhisky.whisky }),
					selectedWhisky.pointsCount ? _react2.default.createElement(
						'span',
						{ className: _Category2.default.pointsCount },
						`${ selectedWhisky.pointsCount } `,
						_react2.default.createElement(
							'span',
							{ className: _Category2.default.pointsCountLabel },
							(value => {
								if (value === 1) {
									return 'bod';
								}

								if (value === 2 || value === 3 || value === 4) {
									return 'body';
								}

								return 'bodů';
							})(selectedWhisky.pointsCount)
						)
					) : null
				));
			}

			let selectedUsers = _data.users.map(user => ({
				name: user.name,
				result: user.results.filter(result => result.categoryName === category.name)[0]
			})).sort((a, b) => a.name.trim().localeCompare(b.name.trim()));

			return _react2.default.createElement(
				'div',
				{ className: _Category2.default.root },
				_react2.default.createElement(
					'h2',
					{ className: _Category2.default.heading },
					category.name
				),
				whiskiesElements && whiskiesElements.length ? _react2.default.createElement(
					'ol',
					{ className: _Category2.default.whiskies },
					whiskiesElements
				) : null,
				_react2.default.createElement(
					'section',
					null,
					_react2.default.createElement(
						'h3',
						{ className: _Category2.default.votesHeading },
						'Um\xEDst\u011Bn\xED'
					),
					selectedUsers.map((user, userIndex) => {
						let elements = user.result.items.map((item, itemIndex) => {
							if (item && typeof item === 'number') {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.whisky },
									_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item }) })
								);
							} else if (item && typeof item === 'object' && typeof item.id === 'number') {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.whisky },
									_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item.id }) })
								);
							} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
								return _react2.default.createElement(
									'li',
									{ key: itemIndex, className: _Category2.default.categoryItem },
									_react2.default.createElement(
										'p',
										{ className: _Category2.default.categoryItemName },
										item.name
									)
								);
							}

							return _react2.default.createElement(
								'li',
								{ key: itemIndex, className: `${ _Category2.default.whisky } ${ _Category2.default.isEmpty }` },
								_react2.default.createElement(
									'p',
									null,
									'\u2013'
								)
							);
						});

						return elements.length ? _react2.default.createElement(
							'section',
							{ key: userIndex, className: _Category2.default.result },
							_react2.default.createElement(
								'header',
								{ className: _Category2.default.resultHeader },
								_react2.default.createElement(
									'h3',
									{ className: _Category2.default.resultHeading },
									_react2.default.createElement(
										'span',
										{ className: _Category2.default.resultHeadingLabel },
										'U\u017Eivatel'
									),
									_react2.default.createElement(
										'a',
										{ href: (0, _href2.default)('uzivatele', user.name) },
										user.name
									)
								)
							),
							_react2.default.createElement(
								'ol',
								null,
								elements
							)
						) : null;
					})
				)
			);
		}
	}]);

	return Category;
}(_react2.default.Component);

exports.default = Category;
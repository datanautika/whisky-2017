'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _WhiskyDetail = require('./WhiskyDetail.css');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _whiskyName3 = require('../utils/whiskyName');

var _whiskyName4 = _interopRequireDefault(_whiskyName3);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _data = require('../data');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 1, maximumFractionDigits: 1 });
const ML_IN_L = 1000;

let name = whisky => {
	var _whiskyName = (0, _whiskyName4.default)(whisky),
	    _whiskyName2 = _slicedToArray(_whiskyName, 4);

	let titlePart1 = _whiskyName2[0],
	    titlePart2 = _whiskyName2[1],
	    titlePart3 = _whiskyName2[2],
	    subtitle = _whiskyName2[3];


	return _react2.default.createElement(
		'h2',
		{ className: _WhiskyDetail2.default.heading },
		_react2.default.createElement(
			'span',
			{ className: _WhiskyDetail2.default.title },
			titlePart1 ? titlePart1 : null,
			titlePart2 ? _react2.default.createElement(
				'b',
				null,
				titlePart2
			) : null,
			titlePart3 ? titlePart3 : null
		),
		_react2.default.createElement(
			'span',
			{ className: _WhiskyDetail2.default.subtitle },
			subtitle ? subtitle : null
		)
	);
};

let parseDate = dateString => {
	let partsCount = dateString.match(/-/g);

	if (partsCount <= 1) {
		return dateString;
	}

	let date = (0, _moment2.default)(dateString);

	if (partsCount > 1) {
		return date.format('Do MMMM YYYY');
	}

	return date.format('MMMM YYYY');
};

let WhiskyDetail = function (_React$Component) {
	_inherits(WhiskyDetail, _React$Component);

	function WhiskyDetail() {
		_classCallCheck(this, WhiskyDetail);

		return _possibleConstructorReturn(this, (WhiskyDetail.__proto__ || Object.getPrototypeOf(WhiskyDetail)).apply(this, arguments));
	}

	_createClass(WhiskyDetail, [{
		key: 'render',
		value: function render() {
			let whisky = this.props.data;
			let regionClass;
			let regionName;
			let countryName = whisky.country;
			let isSingleMalt = whisky.type === 'Single Malt';

			if (whisky.region) {
				regionClass = _WhiskyDetail2.default[whisky.region.toLowerCase()];

				if (whisky.region === 'Islay') {
					regionName = 'Islay';
				} else if (whisky.region === 'Islands') {
					regionName = 'Ostrovy';
				} else if (whisky.region === 'Highlands') {
					regionName = 'Vysočina';
				} else if (whisky.region === 'Lowlands') {
					regionName = 'Nížina';
				} else if (whisky.region === 'Speyside') {
					regionName = 'Speyside';
				} else if (whisky.region === 'Campbeltown') {
					regionName = 'Campbeltown';
				}
			}

			if (whisky.country === 'Scotland') {
				countryName = 'Skotsko';
			}

			if (whisky.country === 'Ireland') {
				countryName = 'Irsko';
			}

			if (whisky.country === 'United States') {
				countryName = 'USA';
			}

			if (whisky.country === 'France') {
				countryName = 'Francie';
			}

			if (whisky.country === 'India') {
				countryName = 'Indie';
			}

			if (whisky.country === 'Canada') {
				countryName = 'Kanada';
			}

			if (whisky.country === 'Czech Republic') {
				countryName = 'Česko';
			}

			if (whisky.country === 'Sweden') {
				countryName = 'Švédsko';
			}

			if (whisky.country === 'Australia') {
				countryName = 'Austrálie';
			}

			if (whisky.country === 'Netherlands') {
				countryName = 'Nizozemí';
			}

			if (whisky.country === 'United Kingdom') {
				countryName = 'Velká Británie';
			}

			if (whisky.country === 'Iceland') {
				countryName = 'Island';
			}

			let selectedUsers = _data.users.filter(user => {
				let use = false;

				user.results.forEach(result => result.items.forEach(item => {
					if (item && typeof item === 'number' && item === whisky.id) {
						use = true;
					} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
						use = true;
					}
				}));

				return use;
			}).map(user => {
				let newUser = _lodash2.default.cloneDeep(user);

				newUser.results = user.results.filter(result => {
					let use = false;

					result.items.forEach(item => {
						if (item && typeof item === 'number' && item === whisky.id) {
							use = true;
						} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
							use = true;
						}
					});

					return use;
				});

				return newUser;
			});

			return _react2.default.createElement(
				'div',
				{ className: _WhiskyDetail2.default.root + (regionClass ? ` ${ regionClass }` : '') + (isSingleMalt ? ` ${ _WhiskyDetail2.default.isSingleMalt }` : '') },
				name(whisky),
				_react2.default.createElement(
					'div',
					{ className: _WhiskyDetail2.default.leftColumn },
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.country ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Zem\u011B'
							),
							_react2.default.createElement(
								'p',
								null,
								countryName
							)
						) : null,
						whisky.region ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Oblast'
							),
							_react2.default.createElement(
								'p',
								null,
								regionName
							)
						) : null
					),
					whisky.distillery || whisky.bottler ? _react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.distillery ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Pal\xEDrna'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.distillery
							)
						) : null,
						whisky.bottler ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'St\xE1\u010D\xEDrna'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.bottler
							)
						) : null
					) : null,
					whisky.age || whisky.vintage || whisky.bottled ? _react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.age ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'St\xE1\u0159\xED'
							),
							_react2.default.createElement(
								'p',
								null,
								`${ whisky.age } let`
							)
						) : null,
						whisky.vintage ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Ro\u010Dn\xEDk'
							),
							_react2.default.createElement(
								'p',
								null,
								parseDate(whisky.vintage)
							)
						) : null,
						whisky.bottled ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Lahvov\xE1no'
							),
							_react2.default.createElement(
								'p',
								null,
								parseDate(whisky.bottled)
							)
						) : null
					) : null,
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.strength ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'S\xEDla'
							),
							_react2.default.createElement(
								'p',
								null,
								`${ NUMBER_FORMAT.format(whisky.strength) } %`
							)
						) : null,
						whisky.size ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Objem'
							),
							_react2.default.createElement(
								'p',
								null,
								`${ whisky.size * ML_IN_L } ml`
							)
						) : null,
						whisky.caskNumber && whisky.caskType ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Sud'
							),
							_react2.default.createElement(
								'p',
								null,
								`${ whisky.caskNumber } (${ whisky.caskType })`
							)
						) : null,
						!whisky.caskNumber && whisky.caskType ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.rowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Sud'
							),
							_react2.default.createElement(
								'p',
								null,
								`${ whisky.caskType }`
							)
						) : null
					),
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						_react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.wideRowItem },
							_react2.default.createElement(
								'ul',
								{ className: _WhiskyDetail2.default.additionalProperties },
								whisky.isUncolored ? _react2.default.createElement(
									'li',
									null,
									'Nebarven\xE1'
								) : null,
								whisky.isNonChillfiltered ? _react2.default.createElement(
									'li',
									null,
									'Nefiltrovan\xE1 za studena'
								) : null,
								whisky.isCaskStrength ? _react2.default.createElement(
									'li',
									null,
									'V sudov\xE9 s\xEDle'
								) : null,
								whisky.isSingleCask ? _react2.default.createElement(
									'li',
									null,
									'Jednosudov\xE1'
								) : null
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: _WhiskyDetail2.default.row },
						whisky.links && whisky.links.length ? _react2.default.createElement(
							'div',
							{ className: _WhiskyDetail2.default.wideRowItem },
							_react2.default.createElement(
								'h4',
								{ className: _WhiskyDetail2.default.attributeHeading },
								'Odkazy'
							),
							_react2.default.createElement(
								'p',
								null,
								whisky.links.map((link, index) => _react2.default.createElement(
									'a',
									{ key: index, href: link },
									link
								))
							)
						) : null
					)
				),
				whisky.image ? _react2.default.createElement(
					'figure',
					{ className: _WhiskyDetail2.default.rightColumn, style: {
							backgroundImage: `url("${ (0, _href2.default)('assets', whisky.image) }")`
						} },
					_react2.default.createElement('img', { src: (0, _href2.default)('assets', whisky.image), alt: '' })
				) : null,
				_react2.default.createElement(
					'section',
					{ className: _WhiskyDetail2.default.fullColumn },
					_react2.default.createElement(
						'h3',
						{ className: _WhiskyDetail2.default.votesHeading },
						'Um\xEDst\u011Bn\xED'
					),
					selectedUsers && selectedUsers.length ? selectedUsers.map((user, userIndex) => _react2.default.createElement(
						'section',
						{ key: userIndex, className: _WhiskyDetail2.default.result },
						_react2.default.createElement(
							'header',
							{ className: _WhiskyDetail2.default.resultHeader },
							_react2.default.createElement(
								'h3',
								{ className: _WhiskyDetail2.default.resultHeading },
								_react2.default.createElement(
									'span',
									{ className: _WhiskyDetail2.default.resultHeadingLabel },
									'U\u017Eivatel'
								),
								_react2.default.createElement(
									'a',
									{ href: (0, _href2.default)('uzivatele', user.name) },
									user.name
								)
							)
						),
						_react2.default.createElement(
							'ol',
							null,
							user.results.map((result, resultIndex) => {
								let category = _lodash2.default.find(_data.categories, { name: result.categoryName });
								let points = 0;

								result.items.forEach((item, itemIndex) => {
									if (item && typeof item === 'number' && item === whisky.id) {
										points += category.points[itemIndex];
									} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
										points += category.points[itemIndex];
									}
								});

								return _react2.default.createElement(
									'li',
									{ key: resultIndex, className: _WhiskyDetail2.default.whisky },
									_react2.default.createElement(
										'a',
										{ href: (0, _href2.default)('kategorie', category.id) },
										result.categoryName
									),
									points ? _react2.default.createElement(
										'span',
										{ className: _WhiskyDetail2.default.pointsCount },
										`${ points } `,
										_react2.default.createElement(
											'span',
											{ className: _WhiskyDetail2.default.pointsCountLabel },
											(value => {
												if (value === 1) {
													return 'bod';
												}

												if (value === 2 || value === 3 || value === 4) {
													return 'body';
												}

												return 'bodů';
											})(points)
										)
									) : null
								);
							})
						)
					)) : null
				)
			);
		}
	}]);

	return WhiskyDetail;
}(_react2.default.Component);

exports.default = WhiskyDetail;
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _WhiskyName = require('./WhiskyName.css');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _whiskyName3 = require('../utils/whiskyName');

var _whiskyName4 = _interopRequireDefault(_whiskyName3);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = url => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new _BrowserRouter2.default(_router2.default);

let WhiskyName = function (_React$Component) {
	_inherits(WhiskyName, _React$Component);

	function WhiskyName() {
		var _temp, _this, _ret;

		_classCallCheck(this, WhiskyName);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (WhiskyName.__proto__ || Object.getPrototypeOf(WhiskyName)).call(this, ...args)), _this), _this.handleClick = event => {
			if (event.button !== 1) {
				let url = _this.refs.root.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(WhiskyName, [{
		key: 'shouldComponentUpdate',
		value: function shouldComponentUpdate(newProps) {
			let shouldUpdate = !this.props.data.links && !newProps.data.links || this.props.data.links && newProps.data.links && this.props.data.links[0] !== newProps.data.links[0] || this.props.data.age !== newProps.data.age || this.props.data.batch !== newProps.data.batch || this.props.data.bottled !== newProps.data.bottled || this.props.data.bottledFor !== newProps.data.bottledFor || this.props.data.bottler !== newProps.data.bottler || this.props.data.bottles !== newProps.data.bottles || this.props.data.brand !== newProps.data.brand || this.props.data.caskNumber !== newProps.data.caskNumber || this.props.data.caskType !== newProps.data.caskType ||
			// this.props.data.categoryGroup !== newProps.data.categoryGroup ||
			// this.props.data.categoryName !== newProps.data.categoryName ||
			this.props.data.country !== newProps.data.country || this.props.data.distillery !== newProps.data.distillery || this.props.data.edition !== newProps.data.edition || this.props.data.id !== newProps.data.id ||
			// this.props.data.image !== newProps.data.image ||
			this.props.data.isCaskStrength !== newProps.data.isCaskStrength || this.props.data.isNonChillfiltered !== newProps.data.isNonChillfiltered || this.props.data.isSingleCask !== newProps.data.isSingleCask || this.props.data.isUncolored !== newProps.data.isUncolored || this.props.data.name !== newProps.data.name ||
			// this.props.data.pointsCount !== newProps.data.pointsCount ||
			this.props.data.region !== newProps.data.region || this.props.data.size !== newProps.data.size || this.props.data.strength !== newProps.data.strength || this.props.data.type !== newProps.data.type ||
			// this.props.data.userId !== newProps.data.userId ||
			this.props.data.vintage !== newProps.data.vintage;

			return shouldUpdate;
		}
	}, {
		key: 'render',
		value: function render() {
			let whisky = this.props.data;
			let isSingleMalt = whisky.type === 'Single Malt';

			if (!whisky) {
				return null;
			}

			var _whiskyName = (0, _whiskyName4.default)(whisky),
			    _whiskyName2 = _slicedToArray(_whiskyName, 4);

			let titlePart1 = _whiskyName2[0],
			    titlePart2 = _whiskyName2[1],
			    titlePart3 = _whiskyName2[2],
			    subtitle = _whiskyName2[3];

			let regionClass;

			if (whisky.region) {
				regionClass = _WhiskyName2.default[whisky.region.toLowerCase()];
			}

			return _react2.default.createElement(
				'a',
				{ ref: 'root', className: _WhiskyName2.default.root + (regionClass ? ` ${ regionClass }` : '') + (isSingleMalt ? ` ${ _WhiskyName2.default.isSingleMalt }` : ''), href: (0, _href2.default)('whisky', whisky.id), onClick: this.handleClick },
				_react2.default.createElement(
					'span',
					{ className: _WhiskyName2.default.title },
					titlePart1 ? titlePart1 : null,
					titlePart2 ? _react2.default.createElement(
						'b',
						null,
						titlePart2
					) : null,
					titlePart3 ? titlePart3 : null
				),
				_react2.default.createElement(
					'span',
					{ className: _WhiskyName2.default.subtitle },
					subtitle ? subtitle : null
				)
			);
		}
	}]);

	return WhiskyName;
}(_react2.default.Component);

exports.default = WhiskyName;
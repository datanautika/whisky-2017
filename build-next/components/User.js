'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _User = require('./User.css');

var _User2 = _interopRequireDefault(_User);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _data = require('../data');

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = url => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new _BrowserRouter2.default(_router2.default);

let User = function (_React$Component) {
	_inherits(User, _React$Component);

	function User() {
		var _temp, _this, _ret;

		_classCallCheck(this, User);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (User.__proto__ || Object.getPrototypeOf(User)).call(this, ...args)), _this), _this.handleClick = event => {
			if (event.button !== 1) {
				let url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(User, [{
		key: 'render',
		value: function render() {
			let user = this.props.data;

			let categoriesElements = user.results.map((result, resultIndex) => {
				let itemsElements = result.items.map((item, itemIndex) => {
					if (item && typeof item === 'number') {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item }) })
						);
					} else if (item && typeof item === 'object' && typeof item.id === 'number') {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(_WhiskyName2.default, { key: itemIndex, data: _lodash2.default.find(_data.whiskies, { id: item.id }) }),
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemDescription },
								item.description
							)
						);
					} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
						return _react2.default.createElement(
							'li',
							{ key: itemIndex, className: _User2.default.categoryItem },
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemName },
								item.name
							),
							_react2.default.createElement(
								'p',
								{ className: _User2.default.categoryItemDescription },
								item.description
							),
							item.links && item.links.length ? _react2.default.createElement(
								'ul',
								{ className: _User2.default.categoryItemLinks },
								item.links.map((link, index) => _react2.default.createElement(
									'li',
									{ key: index },
									_react2.default.createElement(
										'a',
										{ href: link },
										link
									)
								))
							) : null
						);
					}

					return _react2.default.createElement(
						'li',
						{ key: itemIndex, className: `${ _User2.default.categoryItem } ${ _User2.default.isEmpty }` },
						_react2.default.createElement(
							'p',
							null,
							'\u2013'
						)
					);
				});

				return _react2.default.createElement(
					'section',
					{ className: _User2.default.category, key: resultIndex, onClick: this.handleClick },
					_react2.default.createElement(
						'h3',
						{ className: _User2.default.categoryHeading },
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('kategorie', _lodash2.default.find(_data.categories, { name: result.categoryName }).id) },
							result.categoryName
						)
					),
					itemsElements && itemsElements.length ? _react2.default.createElement(
						'ol',
						{ className: _User2.default.categoryResults },
						itemsElements
					) : _react2.default.createElement(
						'p',
						null,
						'\u2013'
					)
				);
			});

			let tempUsers = [];

			_data.users.forEach((otherUser, otherUserIndex) => {
				if (otherUser !== user) {
					tempUsers.push({
						id: otherUser.id,
						name: otherUser.name,
						similarity: user.similarities[otherUserIndex]
					});
				}
			});

			tempUsers.sort((tempUser1, tempUser2) => tempUser2.similarity - tempUser1.similarity);

			let usersElements = user.similarities.map(similarity => similarity.userId === user.id ? null : _react2.default.createElement(
				'li',
				{ className: _User2.default.result, key: similarity.userId },
				_react2.default.createElement(
					'div',
					{ className: _User2.default.resultHeader },
					_react2.default.createElement(
						'h4',
						{ className: _User2.default.resultHeading },
						_react2.default.createElement(
							'span',
							{ className: _User2.default.resultHeadingLabel },
							'U\u017Eivatel'
						),
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('uzivatele', similarity.userName) },
							`${ similarity.userName }`
						)
					),
					_react2.default.createElement(
						'div',
						{ className: _User2.default.points },
						_react2.default.createElement(
							'span',
							{ className: `${ _User2.default.resultHeadingLabel } ${ _User2.default.isRightAligned }` },
							'Kosinov\xE1 podobnost \xD7 100'
						),
						_react2.default.createElement(
							'span',
							{ className: _User2.default.pointsCount },
							`${ NUMBER_FORMAT.format(similarity.cosineSimilarity * 100) }`
						)
					)
				)
			));

			return _react2.default.createElement(
				'div',
				{ className: _User2.default.root },
				_react2.default.createElement(
					'h2',
					{ className: _User2.default.nameHeading },
					`${ user.name }`
				),
				categoriesElements,
				_react2.default.createElement(
					'section',
					null,
					_react2.default.createElement(
						'h3',
						{ className: _User2.default.categoryHeading },
						'Nejpodobn\u011Bji hlasovali'
					),
					_react2.default.createElement(
						'ol',
						null,
						usersElements
					)
				)
			);
		}
	}]);

	return User;
}(_react2.default.Component);

exports.default = User;
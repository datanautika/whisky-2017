'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ayu = require('ayu');

var _ayu2 = _interopRequireDefault(_ayu);

var _WhiskyList = require('./WhiskyList.css');

var _WhiskyList2 = _interopRequireDefault(_WhiskyList);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _data = require('../data');

var _whiskyName5 = require('../utils/whiskyName');

var _whiskyName6 = _interopRequireDefault(_whiskyName5);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
const WHOLE_NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', { minimumFractionDigits: 0, maximumFractionDigits: 0 });

let cache = {};

let WhiskyList = function (_React$Component) {
	_inherits(WhiskyList, _React$Component);

	function WhiskyList() {
		_classCallCheck(this, WhiskyList);

		return _possibleConstructorReturn(this, (WhiskyList.__proto__ || Object.getPrototypeOf(WhiskyList)).apply(this, arguments));
	}

	_createClass(WhiskyList, [{
		key: 'render',
		value: function render() {
			let routeStreamValue = _routeStream2.default.get();
			let group = '';
			let sort = 'name';
			let groupName;

			if (routeStreamValue.group === 'kategorie') {
				group = 'category';
				groupName = 'Kategorie';
			}

			if (routeStreamValue.group === 'oblast') {
				group = 'region';
				groupName = 'Oblast';
			}

			if (routeStreamValue.group === 'palirna') {
				group = 'distillery';
				groupName = 'Palírna';
			}

			if (routeStreamValue.sort === 'nazev') {
				sort = 'name';
			}

			if (routeStreamValue.sort === 'pocet-bodu') {
				sort = 'points-sum';
			}

			if (routeStreamValue.sort === 'prumerny-pocet-bodu') {
				sort = 'points-mean';
			}

			if (routeStreamValue.sort === 'upraveny-pocet-bodu') {
				sort = 'adjusted-points-sum';
			}

			if (routeStreamValue.sort === 'pocet-lahvi') {
				sort = 'whiskies-sum';
			}

			if (sort !== 'name' && sort !== 'points-sum' && sort !== 'points-mean' && sort !== 'adjusted-points-sum' && sort !== 'whiskies-sum' || group !== 'region' && sort === 'adjusted-points-sum' || !group && sort !== 'points-sum') {
				sort = 'name';
			}

			let cacheId = `${ group }:${ sort }`;
			let results;

			if (cache[cacheId]) {
				results = cache[cacheId];
			} else {
				let model = {
					voteIds: new _ayu2.default.Dimension({
						variable: 'id',
						scale: _ayu2.default.NOMINAL_SCALE
					})
				};

				if (group) {
					model[group] = new _ayu2.default.Dimension({
						variable: group === 'category' ? 'categoryName' : group,
						scale: _ayu2.default.NOMINAL_SCALE,
						group: true
					});
				}

				results = new _ayu2.default.DataModel(model, _data.votes);

				if (group) {
					results.filter(value => {
						if (group === 'distillery' && value.groups[group]) {
							return value.groups[group].split(/(\s*,\s*)|(\s&\s)/).length === 1;
						}

						return value.groups[group];
					});
				}

				results.map(value => {
					let newValue = {
						groups: _lodash2.default.cloneDeep(value.groups)
					};

					if (group) {
						newValue.whiskies = value.voteIds.map(voteId => _lodash2.default.cloneDeep(_lodash2.default.find(_data.votes, { id: voteId })));
					} else {
						newValue.whisky = _lodash2.default.cloneDeep(_lodash2.default.find(_data.votes, { id: value.voteIds }));
					}

					if (group) {
						newValue[group] = newValue.groups[group];
						newValue.pointsSum = newValue.whiskies[0].pointsCount;
						newValue.whiskies[0].id = newValue.whiskies[0].whiskyId;

						delete newValue.whiskies[0].whiskyId;

						for (let i = 1; i < newValue.whiskies.length; i++) {
							let deleteWhisky = false;

							newValue.pointsSum += newValue.whiskies[i].pointsCount;
							newValue.whiskies[i].id = newValue.whiskies[i].whiskyId;

							delete newValue.whiskies[i].whiskyId;

							for (let j = 0; j < i; j++) {
								if (newValue.whiskies[j].id === newValue.whiskies[i].id) {
									newValue.whiskies[j].pointsCount += newValue.whiskies[i].pointsCount;
									deleteWhisky = true;

									break;
								}
							}

							if (deleteWhisky) {
								newValue.whiskies.splice(i, 1);

								i--;
							}
						}

						newValue.whiskiesCount = newValue.whiskies.length;
						newValue.pointsMean = newValue.pointsSum / newValue.whiskiesCount;

						if (group === 'region') {
							newValue.adjustedPointsSum = newValue.pointsSum;

							if (value.groups.region === 'Speyside') {
								newValue.adjustedPointsSum /= 48;
								newValue.region = 'Speyside';
							}

							if (value.groups.region === 'Highlands') {
								newValue.adjustedPointsSum /= 34;
								newValue.region = 'Vysočina';
							}

							if (value.groups.region === 'Lowlands') {
								newValue.adjustedPointsSum /= 9;
								newValue.region = 'Nížina';
							}

							if (value.groups.region === 'Islay') {
								newValue.adjustedPointsSum /= 8;
								newValue.region = 'Islay';
							}

							if (value.groups.region === 'Islands') {
								newValue.adjustedPointsSum /= 9;
								newValue.region = 'Ostrovy';
							}

							if (value.groups.region === 'Campbeltown') {
								newValue.adjustedPointsSum /= 3;
								newValue.region = 'Campbeltown';
							}
						}

						newValue.whiskies.sort((a, b) => {
							let result = b.pointsCount - a.pointsCount;

							if (result !== 0) {
								return result;
							}

							return (0, _whiskyName6.default)(a).join(' ').trim().localeCompare((0, _whiskyName6.default)(b).join(' ').trim());
						});
					}

					return newValue;
				});

				// results are not grouped, but we still have to combine whiskies
				if (!group) {
					results.values[0].pointsSum = results.values[0].whisky.pointsCount;
					results.values[0].whisky.id = results.values[0].whisky.whiskyId;

					delete results.values[0].whisky.whiskyId;

					for (let i = 1; i < results.values.length; i++) {
						let deleteWhisky = false;

						results.values[i].pointsSum = results.values[i].whisky.pointsCount;
						results.values[i].whisky.id = results.values[i].whisky.whiskyId;

						delete results.values[i].whisky.whiskyId;

						for (let j = 0; j < i; j++) {
							if (results.values[j].whisky.id === results.values[i].whisky.id) {
								results.values[j].pointsSum += results.values[i].whisky.pointsCount;
								deleteWhisky = true;

								break;
							}
						}

						if (deleteWhisky) {
							results.values.splice(i, 1);

							i--;
						}
					}
				}

				if (sort) {
					results.sort((a, b) => {
						if (group) {
							let variable = 'pointsSum';

							if (sort === 'name') {
								variable = group;

								return a[variable].localeCompare(b[variable]);
							}

							if (sort === 'points-mean') {
								variable = 'pointsMean';
							}

							if (sort === 'adjusted-points-sum') {
								variable = 'adjustedPointsSum';
							}

							if (sort === 'whiskies-sum') {
								variable = 'whiskiesCount';
							}

							return b[variable] - a[variable];
						}

						if (sort === 'name') {
							var _whiskyName = (0, _whiskyName6.default)(a.whisky),
							    _whiskyName2 = _slicedToArray(_whiskyName, 4);

							let titlePart1A = _whiskyName2[0],
							    titlePart2A = _whiskyName2[1],
							    titlePart3A = _whiskyName2[2],
							    subtitleA = _whiskyName2[3];

							var _whiskyName3 = (0, _whiskyName6.default)(b.whisky),
							    _whiskyName4 = _slicedToArray(_whiskyName3, 4);

							let titlePart1B = _whiskyName4[0],
							    titlePart2B = _whiskyName4[1],
							    titlePart3B = _whiskyName4[2],
							    subtitleB = _whiskyName4[3];


							if (!titlePart1A) {
								titlePart1A = titlePart2A;
							}

							if (!titlePart1B) {
								titlePart1B = titlePart2B;
							}

							let result = titlePart1A.trim().localeCompare(titlePart1B.trim());

							if (result !== 0) {
								return result;
							}

							if (a.whisky.age && b.whisky.age) {
								return a.whisky.age - b.whisky.age;
							}

							return `${ titlePart2A } ${ titlePart3A } ${ subtitleA }`.trim().localeCompare(`${ titlePart2B } ${ titlePart3B } ${ subtitleB }`.trim());
						}

						return b.pointsSum - a.pointsSum;
					});
				}

				cache[cacheId] = results;
			}

			return _react2.default.createElement(
				'div',
				{ className: _WhiskyList2.default.root },
				_react2.default.createElement(
					'h2',
					{ className: _WhiskyList2.default.heading },
					'Whisky'
				),
				_react2.default.createElement(
					'div',
					{ className: _WhiskyList2.default.filters },
					_react2.default.createElement(
						'div',
						{ className: _WhiskyList2.default.filter },
						_react2.default.createElement(
							'h4',
							{ className: _WhiskyList2.default.filtersHeading },
							'Rozt\u0159\xEDdit podle'
						),
						_react2.default.createElement(
							'ol',
							{ className: _WhiskyList2.default.filterList, onClick: this.handleGroupFilterClick },
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: group === 'category' ? _WhiskyList2.default.isSelected : '', 'data-group': 'category', href: (0, _href2.default)('whisky', 'kategorie', routeStreamValue.sort) },
									'Kategorie'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: group === 'region' ? _WhiskyList2.default.isSelected : '', 'data-group': 'region', href: (0, _href2.default)('whisky', 'oblast', routeStreamValue.sort) },
									'Oblasti'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: group === 'distillery' ? _WhiskyList2.default.isSelected : '', 'data-group': 'distillery', href: (0, _href2.default)('whisky', 'palirna', routeStreamValue.sort) },
									'Pal\xEDrny'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ 'data-group': 'distillery', href: (0, _href2.default)('whisky', '-', routeStreamValue.sort) },
									'(zru\u0161it)'
								)
							)
						)
					),
					_react2.default.createElement(
						'div',
						{ className: _WhiskyList2.default.filter },
						_react2.default.createElement(
							'h4',
							{ className: _WhiskyList2.default.filtersHeading },
							'Se\u0159adit podle'
						),
						_react2.default.createElement(
							'ol',
							{ className: _WhiskyList2.default.filterList, onClick: this.handleSortFilterClick },
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: sort === 'name' ? _WhiskyList2.default.isSelected : '', 'data-sort': 'name', href: (0, _href2.default)('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'nazev') },
									'N\xE1zvu'
								)
							),
							group ? _react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: sort === 'whiskies-sum' ? _WhiskyList2.default.isSelected : '', 'data-sort': 'whiskies-sum', href: (0, _href2.default)('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'pocet-lahvi') },
									'Po\u010Dtu lahv\xED'
								)
							) : null,
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: sort === 'points-sum' ? _WhiskyList2.default.isSelected : '', 'data-sort': 'points-sum', href: (0, _href2.default)('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'pocet-bodu') },
									'Po\u010Dtu bod\u016F'
								)
							),
							group === 'region' ? _react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: sort === 'adjusted-points-sum' ? _WhiskyList2.default.isSelected : '', 'data-sort': 'adjusted-points-sum', href: (0, _href2.default)('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'upraveny-pocet-bodu') },
									'Upraven\xE9ho po\u010Dtu bod\u016F'
								)
							) : null,
							group ? _react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'a',
									{ className: sort === 'points-mean' ? _WhiskyList2.default.isSelected : '', 'data-sort': 'points-mean', href: (0, _href2.default)('whisky', routeStreamValue.group ? routeStreamValue.group : '-', 'prumerny-pocet-bodu') },
									'Pr\u016Fm\u011Brn\xE9ho po\u010Dtu bod\u016F'
								)
							) : null
						)
					)
				),
				group ? results.values.map((value, valueIndex) => _react2.default.createElement(
					'section',
					{ className: _WhiskyList2.default.result, key: valueIndex },
					_react2.default.createElement(
						'header',
						{ className: _WhiskyList2.default.resultHeader },
						_react2.default.createElement(
							'h3',
							{ className: _WhiskyList2.default.resultHeading },
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.resultHeadingLabel },
								groupName
							),
							group === 'category' ? _react2.default.createElement(
								'a',
								{ href: (0, _href2.default)('kategorie', _lodash2.default.find(_data.categories, { name: value[group] }).id) },
								value[group]
							) : value[group]
						),
						_react2.default.createElement(
							'div',
							{ className: _WhiskyList2.default.points },
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.resultHeadingLabel },
								'Po\u010Det lahv\xED'
							),
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.pointsCount },
								WHOLE_NUMBER_FORMAT.format(value.whiskiesCount)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: _WhiskyList2.default.points },
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.resultHeadingLabel },
								'Po\u010Det bod\u016F'
							),
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.pointsCount },
								WHOLE_NUMBER_FORMAT.format(value.pointsSum)
							)
						),
						typeof value.adjustedPointsSum === 'undefined' ? null : _react2.default.createElement(
							'div',
							{ className: _WhiskyList2.default.points },
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.resultHeadingLabel },
								'Upraven\xFD po\u010Det bod\u016F'
							),
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.pointsCount },
								NUMBER_FORMAT.format(value.adjustedPointsSum)
							)
						),
						_react2.default.createElement(
							'div',
							{ className: _WhiskyList2.default.points },
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.resultHeadingLabel },
								'Pr\u016Fm\u011Brn\xFD po\u010Det bod\u016F'
							),
							_react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.pointsCount },
								NUMBER_FORMAT.format(value.pointsMean)
							)
						)
					),
					_react2.default.createElement(
						'ol',
						null,
						value.whiskies.map(whisky => _react2.default.createElement(
							'li',
							{ key: whisky.id, className: _WhiskyList2.default.whisky },
							_react2.default.createElement(_WhiskyName2.default, { data: whisky }),
							/*whisky.pointsCount ? <span className={styles.pointsCount}>{`${whisky.pointsCount} `}<span className={styles.pointsCountLabel}>{((pointsCount) => {
       if (pointsCount === 1) {
       	return 'bod';
       }
       	if (pointsCount === 2 || pointsCount === 3 || pointsCount === 4) {
       	return 'body';
       }
       	return 'bodů';
       })(whisky.pointsCount)}</span></span> : null*/whisky.pointsCount ? _react2.default.createElement(
								'span',
								{ className: _WhiskyList2.default.pointsCount },
								whisky.pointsCount
							) : null
						))
					)
				)) : _react2.default.createElement(
					'ol',
					null,
					results.values.map(value => _react2.default.createElement(
						'li',
						{ key: value.whisky.id, className: _WhiskyList2.default.whisky },
						_react2.default.createElement(_WhiskyName2.default, { data: value.whisky }),
						/*value.pointsSum ? <span className={styles.pointsCount}>{`${value.pointsSum} `}<span className={styles.pointsCountLabel}>{((pointsSum) => {
      if (pointsSum === 1) {
      	return 'bod';
      }
      	if (pointsSum === 2 || pointsSum === 3 || pointsSum === 4) {
      	return 'body';
      }
      	return 'bodů';
      })(value.pointsSum)}</span></span> : null*/value.pointsSum ? _react2.default.createElement(
							'span',
							{ className: _WhiskyList2.default.pointsCount },
							value.pointsSum
						) : null
					))
				)
			);
		}
	}]);

	return WhiskyList;
}(_react2.default.Component);

exports.default = WhiskyList;
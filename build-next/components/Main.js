'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _Main = require('./Main.css');

var _Main2 = _interopRequireDefault(_Main);

var _User = require('./User');

var _User2 = _interopRequireDefault(_User);

var _WhiskyList = require('./WhiskyList');

var _WhiskyList2 = _interopRequireDefault(_WhiskyList);

var _WhiskyDetail = require('./WhiskyDetail');

var _WhiskyDetail2 = _interopRequireDefault(_WhiskyDetail);

var _WhiskyName = require('./WhiskyName');

var _WhiskyName2 = _interopRequireDefault(_WhiskyName);

var _Category = require('./Category');

var _Category2 = _interopRequireDefault(_Category);

var _data = require('../data');

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

var _routeStream = require('../streams/routeStream');

var _routeStream2 = _interopRequireDefault(_routeStream);

var _whiskyName5 = require('../utils/whiskyName');

var _whiskyName6 = _interopRequireDefault(_whiskyName5);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// const MAILTO_REGEX = /^mailto:/;
// const ROUTE_LINK_REGEX = /^\//;
const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = url => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new _BrowserRouter2.default(_router2.default);

let userList = () => _react2.default.createElement(
	'section',
	{ className: _Main2.default.users },
	_react2.default.createElement(
		'h2',
		{ className: _Main2.default.heading },
		_react2.default.createElement(
			'a',
			{ href: (0, _href2.default)('uzivatele') },
			'U\u017Eivatel\xE9'
		)
	),
	_react2.default.createElement(
		'ul',
		null,
		_data.users.sort((a, b) => a.name.trim().localeCompare(b.name.trim())).map((user, index) => _react2.default.createElement(
			'li',
			{ key: index },
			_react2.default.createElement(
				'a',
				{ href: (0, _href2.default)('uzivatele', user.name) },
				user.name
			)
		))
	)
);

let whiskyList = () => _react2.default.createElement(
	'section',
	{ className: _Main2.default.whiskies },
	_react2.default.createElement(
		'h2',
		{ className: _Main2.default.heading },
		_react2.default.createElement(
			'a',
			{ href: (0, _href2.default)('whisky') },
			'Whisky'
		)
	),
	_react2.default.createElement(
		'ul',
		null,
		_data.whiskies.sort((a, b) => {
			var _whiskyName = (0, _whiskyName6.default)(a),
			    _whiskyName2 = _slicedToArray(_whiskyName, 4);

			let titlePart1A = _whiskyName2[0],
			    titlePart2A = _whiskyName2[1],
			    titlePart3A = _whiskyName2[2],
			    subtitleA = _whiskyName2[3];

			var _whiskyName3 = (0, _whiskyName6.default)(b),
			    _whiskyName4 = _slicedToArray(_whiskyName3, 4);

			let titlePart1B = _whiskyName4[0],
			    titlePart2B = _whiskyName4[1],
			    titlePart3B = _whiskyName4[2],
			    subtitleB = _whiskyName4[3];


			if (!titlePart1A) {
				titlePart1A = titlePart2A;
			}

			if (!titlePart1B) {
				titlePart1B = titlePart2B;
			}

			let result = titlePart1A.trim().localeCompare(titlePart1B.trim());

			if (result !== 0) {
				return result;
			}

			if (a.age && b.age) {
				return a.age - b.age;
			}

			return `${ titlePart2A } ${ titlePart3A } ${ subtitleA }`.trim().localeCompare(`${ titlePart2B } ${ titlePart3B } ${ subtitleB }`.trim());
		}).map((whisky, index) => _react2.default.createElement(
			'li',
			{ key: index },
			_react2.default.createElement(_WhiskyName2.default, { key: index, data: whisky })
		))
	)
);

let categoriesList = () => _react2.default.createElement(
	'section',
	{ className: _Main2.default.categories },
	_react2.default.createElement(
		'h2',
		{ className: _Main2.default.heading },
		_react2.default.createElement(
			'a',
			{ href: (0, _href2.default)('kategorie') },
			'Kategorie'
		)
	),
	_react2.default.createElement(
		'ul',
		null,
		_data.categories.map((category, index) => _react2.default.createElement(
			'li',
			{ key: index },
			_react2.default.createElement(
				'a',
				{ href: (0, _href2.default)('kategorie', category.id) },
				category.name
			)
		))
	)
);

let Main = function (_React$Component) {
	_inherits(Main, _React$Component);

	function Main() {
		var _temp, _this, _ret;

		_classCallCheck(this, Main);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (Main.__proto__ || Object.getPrototypeOf(Main)).call(this, ...args)), _this), _this.handleClick = event => {
			if (event.button !== 1) {
				let url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					if (/whisky\/.+\//.test(url)) {
						browserRouter.navigate(url, { resetScrollPosition: false });
					} else {
						browserRouter.navigate(url);
					}
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}

			/*if (!(this.props.link || MAILTO_REGEX.test(this.props.link))) {
   	event.preventDefault();
   		if (this.props.handleClick) {
   		this.props.handleClick();
   	}
   } else if (ROUTE_LINK_REGEX.test(this.props.link) && this.props.useRouter !== false) {
   	event.preventDefault();
   		browserRouter.navigate(this.props.link);
   }*/
		}, _temp), _possibleConstructorReturn(_this, _ret);
	}

	_createClass(Main, [{
		key: 'render',
		value: function render() {
			var _routeStream$get = _routeStream2.default.get();

			let page = _routeStream$get.page,
			    subpage = _routeStream$get.subpage;

			let elements = null;

			if (!page) {
				elements = _react2.default.createElement(
					'div',
					{ className: _Main2.default.columns },
					_react2.default.createElement(
						'section',
						{ className: _Main2.default.help },
						_react2.default.createElement(
							'h2',
							{ className: _Main2.default.heading },
							'N\xE1pov\u011Bda'
						),
						_react2.default.createElement(
							'p',
							null,
							'Whisky jsou barevn\u011B ozna\u010Den\xE9 podle oblasti a typu:'
						),
						_react2.default.createElement(
							'ul',
							{ className: _Main2.default.whiskyLabels },
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.islay }` },
									'Islay'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.islay } ${ _Main2.default.isSingleMalt }` },
									'Islay, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.islands }` },
									'Ostrovy'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.islands } ${ _Main2.default.isSingleMalt }` },
									'Ostrovy, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.lowlands }` },
									'N\xED\u017Eina'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.lowlands } ${ _Main2.default.isSingleMalt }` },
									'N\xED\u017Eina, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.highlands }` },
									'Vyso\u010Dina'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.highlands } ${ _Main2.default.isSingleMalt }` },
									'Vyso\u010Dina, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.speyside }` },
									'Speyside'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.speyside } ${ _Main2.default.isSingleMalt }` },
									'Speyside, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.campbeltown }` },
									'Campbeltown'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.campbeltown } ${ _Main2.default.isSingleMalt }` },
									'Campbeltown, single malt'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel }` },
									'Jin\xE1 oblast'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									'span',
									{ className: `${ _Main2.default.whiskyLabel } ${ _Main2.default.isSingleMalt }` },
									'Jin\xE1 oblast, single malt'
								)
							)
						)
					),
					userList(),
					categoriesList(),
					whiskyList()
				);
			}

			if (page === 'uzivatele' && !subpage) {
				elements = userList();
			}

			if (page === 'uzivatele' && subpage) {
				elements = _react2.default.createElement(_User2.default, { data: _lodash2.default.find(_data.users, { name: subpage }) });
			}

			if (page === 'whisky' && typeof subpage !== 'number') {
				elements = _react2.default.createElement(_WhiskyList2.default, null);
			}

			if (page === 'whisky' && typeof subpage === 'number') {
				elements = _react2.default.createElement(_WhiskyDetail2.default, { data: _lodash2.default.find(_data.whiskies, { id: subpage }) });
			}

			if (page === 'kategorie' && !subpage) {
				elements = categoriesList();
			}

			if (page === 'kategorie' && typeof subpage === 'number') {
				elements = _react2.default.createElement(_Category2.default, { data: _lodash2.default.find(_data.categories, { id: subpage }) });
			}

			return _react2.default.createElement(
				'main',
				{ className: _Main2.default.root, onClick: this.handleClick },
				elements
			);
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {
			_routeStream2.default.subscribe(() => {
				this.forceUpdate();
			});
		}
	}]);

	return Main;
}(_react2.default.Component);

exports.default = Main;
'use strict';

require('./internals/polyfills');

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _koaCompress = require('koa-compress');

var _koaCompress2 = _interopRequireDefault(_koaCompress);

var _koaLogger = require('koa-logger');

var _koaLogger2 = _interopRequireDefault(_koaLogger);

var _koaBodyparser = require('koa-bodyparser');

var _koaBodyparser2 = _interopRequireDefault(_koaBodyparser);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _koaStatic = require('koa-static');

var _koaStatic2 = _interopRequireDefault(_koaStatic);

var _appRoot = require('./internals/appRoot');

var _appRoot2 = _interopRequireDefault(_appRoot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

const HTTP_NOT_FOUND = 404;
const HTTP_INTERNAL_ERROR = 500;
const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

let readFile = Promise.promisify(_fs2.default.readFile);
let app = new _koa2.default();
let uriAppRoot = '/whiskyRoku2017'.replace(ROUTE_STRIPPER, '');

console.log('App root dir:', `"${ _appRoot2.default }"`); // eslint-disable-line no-console
console.log('App root URL:', `"${ uriAppRoot }"`); // eslint-disable-line no-console

app.use((0, _koaBodyparser2.default)());
app.use((0, _koaLogger2.default)());
app.use((() => {
	var _ref = _asyncToGenerator(function* (context, next) {
		try {
			yield next();
		} catch (error) {
			console.warn('\nError!\n', error); // eslint-disable-line no-console

			context.status = error.status || HTTP_INTERNAL_ERROR;

			context.body = {
				error: {
					message: error.message
				}
			};

			context.app.emit('error', error, context);
		}
	});

	return function (_x, _x2) {
		return _ref.apply(this, arguments);
	};
})());
app.use((0, _koaStatic2.default)(_path2.default.resolve(_path2.default.join(_appRoot2.default, 'public'))));

// serve app
app.use((() => {
	var _ref2 = _asyncToGenerator(function* (context, next) {
		if (context.method !== 'HEAD' && context.method !== 'GET') {
			yield next();

			return;
		}

		// response is already handled
		if (context.body && context.body !== null || context.status !== HTTP_NOT_FOUND) {
			yield next();

			return;
		}

		context.body = yield readFile(_path2.default.join(_appRoot2.default, 'public/index.html'), 'utf8');

		yield next();
	});

	return function (_x3, _x4) {
		return _ref2.apply(this, arguments);
	};
})());

// compression
app.use((0, _koaCompress2.default)());

const PORT = 8080;

_http2.default.createServer(app.callback()).listen(undefined || PORT);

console.log(`Listening on port ${ undefined || PORT }...`); // eslint-disable-line no-console
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _nconf = require('nconf');

var _nconf2 = _interopRequireDefault(_nconf);

var _secrets = require('./secrets');

var _secrets2 = _interopRequireDefault(_secrets);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import appRoot from '../../internals/appRoot';


_nconf2.default.argv();
// import path from 'path';

_nconf2.default.env();
// nconf.use('file', {file: path.join(appRoot, 'private/credentials.json')});

_nconf2.default.add('secrets', { type: 'literal', store: { secrets: _secrets2.default } });

exports.default = _nconf2.default;
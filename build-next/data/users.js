"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
/* eslint-disable */

let users = exports.users = [{
  "id": 1,
  "name": "jakubmazanec",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [1]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [2, 3, 4]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [1, 5, 6]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [7, 8, 9]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [10, 11, 12]
  }, {
    "categoryName": "Blended whisky",
    "items": [13, 14, 15]
  }, {
    "categoryName": "Blended malt",
    "items": [16, 17]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [18, 19, 20]
  }, {
    "categoryName": "Irsko",
    "items": [21, 22, 23]
  }, {
    "categoryName": "Japonsko",
    "items": [24]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [25, 26, 27]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [28, {
      "name": "Svach\u2019s Old Well History Begins",
      "description": "Single Malt Spirit"
    }, 27]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [29, {
      "name": "Trebitsch Small Batch ",
      "description": "Single Barrel \u2116 3015001, 61,2 %, Fill Date 03-17",
      "links": []
    }, 30]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [31, 32, 33]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "Dukla \u015awitezianka",
      "links": ["https:\/\/www.ratebeer.com\/beer\/dukla-346witezianka\/497805\/"]
    }]
  }]
}, {
  "id": 2,
  "name": "salam15",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [7]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [34, 35, 36]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [37, 38, 39]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [40, 41, 42]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [43, 44, 45]
  }, {
    "categoryName": "Blended whisky",
    "items": [13, 46, 47]
  }, {
    "categoryName": "Blended malt",
    "items": [48, 49, 16]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [39, {
      "name": "Scotch Malt Whisky Society",
      "links": ["https:\/\/www.smws.com\/"]
    }, 51]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [{
      "name": "Pro\u0161vihnut\u00e1 akce na Talisker 25 yo za 175,- EUR na drankdozijn.nl"
    }, 52, 53]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [54, 55, 56]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 3,
  "name": "Hunter",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [50]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [57, 35, 58]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [59, 60, 61]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [62, 63, 40]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [64, 65, 66]
  }, {
    "categoryName": "Blended whisky",
    "items": [67, 68, 69]
  }, {
    "categoryName": "Blended malt",
    "items": [16]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [70, 71, 72]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [73, 74, 27]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [75, 74, 76]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [{
      "name": "Mount Gay Eclipse",
      "links": ["https:\/\/www.rumratings.com\/brands\/563-mount-gay-eclipse"]
    }]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [77]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Plantation SC Haiti XO White Pineau Finish",
      "links": ["https:\/\/www.rumratings.com\/brands\/4209-plantation-haiti-xo-single-cask-white-pineau-finish"]
    }, {
      "name": "Plantation SC Belize XO Grapia Finish",
      "links": ["https:\/\/www.rumratings.com\/brands\/4104-plantation-belize-8-year-grapia-finish"]
    }, {
      "name": "Plantation SC Multi-Island Banyuls Cask Finish",
      "links": ["https:\/\/www.rumratings.com\/brands\/2349-plantation-multi-island"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "Kocour Quarterback",
      "links": ["https:\/\/www.ratebeer.com\/beer\/kocour-70-quarterback\/138373\/"]
    }, {
      "name": "Albrecht India Pale Ale 17",
      "links": ["https:\/\/www.ratebeer.com\/beer\/albrecht-ipa-17\/397624\/"]
    }, {
      "name": "Biskupsk\u00fd pivovar Litom\u011b\u0159ice, sv\u011btl\u00fd le\u017e\u00e1k \u0160t\u011bp\u00e1n",
      "links": ["http:\/\/biskupskypivovar.cz\/?page=nase-pivo"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "Arrogant Bastard Ale, Arrogant Brewing, USA",
      "links": ["https:\/\/www.ratebeer.com\/beer\/arrogant-bastard-ale\/1315\/"]
    }, {
      "name": "Camba Bavaria Imperial IPA, Camba Bavaria, N\u011bmecko",
      "links": ["https:\/\/www.ratebeer.com\/beer\/camba-bavaria-imperial-ipa\/278253\/"]
    }, {
      "name": "Rowing Jack IPA, Ale Browar, Polsko",
      "links": ["https:\/\/www.ratebeer.com\/beer\/alebrowar-rowing-jack\/172656\/"]
    }]
  }]
}, {
  "id": 4,
  "name": "J.K.Ch.",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [50]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [78, 79, 80]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [344, 205, 237]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [81, 42, 82]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [11, 83, 84]
  }, {
    "categoryName": "Blended whisky",
    "items": [15, 14, 85]
  }, {
    "categoryName": "Blended malt",
    "items": [48, 86, 87]
  }, {
    "categoryName": "Grain whisky",
    "items": [88, 89]
  }, {
    "categoryName": "USA a Kanada",
    "items": [90, 71, 91]
  }, {
    "categoryName": "Irsko",
    "items": [92, 93, 94]
  }, {
    "categoryName": "Japonsko",
    "items": [95, 96, 97]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50, 98, 99]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [50]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [100, 101]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [102, 103]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Whiskybran\u00ed"
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 5,
  "name": "Ultimo",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [104]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [105]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [106, 107, 108]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [109, 110, 111]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [112, 104, 113]
  }, {
    "categoryName": "Blended whisky",
    "items": [114]
  }, {
    "categoryName": "Blended malt",
    "items": [115, 116]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": []
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [117, 118, 119]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [120, 121, 122]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [123, 124, 125]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 6,
  "name": "zyki",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [41]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [126, 60, 127]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [41, 40, 81]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [10, 128, 129]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [48]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [130, 131, 132]
  }, {
    "categoryName": "Irsko",
    "items": [133, 134, 21]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50, 27, 135]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": []
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": []
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 7,
  "name": "Walter",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [125]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [125, 37, 136]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": []
  }, {
    "categoryName": "Blended whisky",
    "items": [null, 137]
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": []
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": []
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": []
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [{
      "name": "Martell VS Fine Cognac",
      "description": "Zakoupeno za 419 K\u010d"
    }, {
      "name": "Pampero Aniversario Reserva Exclusiva",
      "description": "Zakoupeno za 488 K\u010d"
    }, {
      "id": 125,
      "name": "Laphroaig 10 yo",
      "description": "Zakoupeno za 787 K\u010d"
    }]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Presidente Marti 15",
      "links": ["https:\/\/www.rumratings.com\/brands\/1506-presidente-15-year"]
    }, {
      "name": "Zacapa Centenario 23 Sistema Solera",
      "links": ["https:\/\/www.rumratings.com\/brands\/853-ron-zacapa-23-solera"]
    }, {
      "name": "Pampero Aniversario Reserva Exclusiva",
      "links": ["https:\/\/www.rumratings.com\/brands\/631-pampero-aniversario-reserva-exclusiva"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 8,
  "name": "nidan",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [138]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [139, 140, 80]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [141, 142, 143]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [144, 145, 146]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [32, 147, 148]
  }, {
    "categoryName": "Blended whisky",
    "items": [149, 150, 103]
  }, {
    "categoryName": "Blended malt",
    "items": [48]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [71]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [151, 152, 73]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [27, 153, 154]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [53, 155, 156]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [157, 158, 159]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Malt whisky yearbook 2017"
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "Samson 12"
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "Kasleel Cuv\u00e9e du Chateau"
    }]
  }]
}, {
  "id": 9,
  "name": "gln",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [33]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [160, 102, 161]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [33, 162, 163]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [26]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": []
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [164]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [102]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "Clock No Idols! 16\u00b0 Cascadian Dark Ale",
      "links": ["https:\/\/www.ratebeer.com\/beer\/clock-no-idols-16%C2%B0-cascadian-dark-ale-limited-edition\/288746\/"]
    }, {
      "name": "Matu\u0161ka Tropical Rocket 17\u00b0",
      "links": ["https:\/\/www.ratebeer.com\/beer\/matuska-tropical-rocket-17%C2%B0\/488456\/"]
    }, {
      "name": "Albrecht 17\u00b0 Ducis Astrologus",
      "links": ["https:\/\/www.ratebeer.com\/beer\/albrecht-17%C2%B0-ducis-astrologus\/494113\/"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "Pauwel Kwak",
      "links": ["https:\/\/www.ratebeer.com\/beer\/pauwel-kwak\/3658\/"]
    }, {
      "name": "Tiny Rebel Clwb Tropicana",
      "links": ["https:\/\/www.ratebeer.com\/beer\/tiny-rebel-clwb-tropicana\/398179\/"]
    }, {
      "name": "Alvinne Sigma",
      "links": ["https:\/\/www.ratebeer.com\/beer\/alvinne-sigma\/247376\/"]
    }]
  }]
}, {
  "id": 10,
  "name": "lalos",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [165]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [4, 2]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [37, 166, 167]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [168, 169, 170]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [165, 171, 33]
  }, {
    "categoryName": "Blended whisky",
    "items": [172, 14, 103]
  }, {
    "categoryName": "Blended malt",
    "items": [173, 174, 175]
  }, {
    "categoryName": "Grain whisky",
    "items": [176, 177, 178]
  }, {
    "categoryName": "USA a Kanada",
    "items": [179, 180, 181]
  }, {
    "categoryName": "Irsko",
    "items": [182, 183]
  }, {
    "categoryName": "Japonsko",
    "items": [172]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50, 27, 74]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [50]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [184]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [182, 103]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 11,
  "name": "Senoy",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [185]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [186, 187, 80]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [188, 189, 125]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [190, 191, 192]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [185, 193, 165]
  }, {
    "categoryName": "Blended whisky",
    "items": [194, 103]
  }, {
    "categoryName": "Blended malt",
    "items": [48, 195, 16]
  }, {
    "categoryName": "Grain whisky",
    "items": [177]
  }, {
    "categoryName": "USA a Kanada",
    "items": [71]
  }, {
    "categoryName": "Irsko",
    "items": [null, 182, 196]
  }, {
    "categoryName": "Japonsko",
    "items": [197]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [198, 199, 135]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [185]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [200]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [201]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Whiskybran\u00ed",
      "links": ["http:\/\/www.thewhiskyshop.cz\/knihy-o-whisky\/224-whiskybrani-svatopluk-buchlovsky.html"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "",
      "links": ["https:\/\/www.rumratings.com\/brands\/1309-a-h-riise-navy-strength-55"]
    }, {
      "name": "",
      "links": ["https:\/\/www.rumratings.com\/brands\/1506-presidente-15-year-rum"]
    }, {
      "name": "",
      "links": ["https:\/\/www.masterofmalt.com\/rum\/oliver-and-oliver\/presidente-marti-anejo-rum\/"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "",
      "links": ["http:\/\/www.beskydskypivovarek.cz\/nase-piva\/"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 12,
  "name": "joy",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [202]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [203, 31, 187]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [204, 205, 206]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [202, 207]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [10, 208, 209]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [48]
  }, {
    "categoryName": "Grain whisky",
    "items": [210, 211]
  }, {
    "categoryName": "USA a Kanada",
    "items": [null, null, 212]
  }, {
    "categoryName": "Irsko",
    "items": [null, 213]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [214, 215, 216]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [204]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": []
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [217]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Dos Maderas PX 5+5",
      "links": ["https:\/\/www.rumratings.com\/brands\/341-dos-maderas-px-5-5"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "Matu\u0161ka Hellcat",
      "links": ["http:\/\/www.pivnici.cz\/pivo\/matuska-hellcat\/"]
    }, {
      "name": "Permon Sherpa IPA",
      "links": ["http:\/\/www.pivnici.cz\/hledat\/piva\/?text=permon"]
    }, {
      "name": "Koutsk\u00e1 des\u00edtka",
      "links": ["http:\/\/www.pivnici.cz\/pivo\/koutska-desitka-nefiltrovana\/"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "Sierra Nevada Torpedo Extra IPA",
      "links": ["http:\/\/www.pivnici.cz\/pivo\/sierra-nevada-torpedo-extra-ipa\/"]
    }, {
      "name": "Gulden Draak",
      "links": ["http:\/\/www.pivnici.cz\/pivo\/gulden-draak\/"]
    }, {
      "name": "Gouden Carolus Tripel",
      "links": ["http:\/\/www.pivnici.cz\/pivo\/gouden-carolus-tripel\/"]
    }]
  }]
}, {
  "id": 13,
  "name": "richie",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [218]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [219, 220, 35]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [39, 221, 222]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [223, 224, 225]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [226, 33, 227]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [195, 48, 16]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [216, 228, 27]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [219, 39, 50]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [229, 230, 231]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [232, 233, 234]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "id": null,
      "name": "Malt Whisky Yearbook 2018",
      "description": "",
      "links": ["http:\/\/www.maltwhiskyyearbook.com\/"]
    }, {
      "id": null,
      "name": "Sv\u011bt whisky",
      "description": "",
      "links": ["https:\/\/www.knizniklub.cz\/knihy\/236230-svet-whisky.html"]
    }, {
      "id": null,
      "name": "101 Whiskies to try before you die (Revised & Updated)",
      "description": "",
      "links": ["https:\/\/www.bookdepository.com\/101-Whiskies-Try-Before-You-Die-Revised--Updated-Ian-Buxton\/9781472242471?ref=grid-view&qid=1510862730407&sr=1-2"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Plantation St.Lucia 2004",
      "links": ["https:\/\/www.rumratings.com\/brands\/5210-plantation-st-lucia-2004"]
    }, {
      "name": "Planation SC Multi Island",
      "links": ["https:\/\/www.rumratings.com\/brands\/2349-plantation-multi-island"]
    }, {
      "name": "A. H. Riise Royal Danish Navy",
      "links": ["https:\/\/www.rumratings.com\/brands\/1307-a-h-riise-royal-danish-navy"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 14,
  "name": "Marvin",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [235, 35, 236]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [237, 126, 1]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [238, 239, 240]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [241, 10, 84]
  }, {
    "categoryName": "Blended whisky",
    "items": [242, 243, 244]
  }, {
    "categoryName": "Blended malt",
    "items": [245, 246, 17]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [null, 247, 248]
  }, {
    "categoryName": "Irsko",
    "items": [249, 250, 23]
  }, {
    "categoryName": "Japonsko",
    "items": [24, 251, 252]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": []
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [253]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [30]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [254]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Whisky (historie, v\u00fdroba, zna\u010dky)",
      "links": ["https:\/\/www.knizniklub.cz\/knihy\/230099-whisky.html"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "English Harbour 1981",
      "links": ["https:\/\/www.rumratings.com\/brands\/1088-english-harbour-1981"]
    }, {
      "name": "Barcelo Imperial 30 Aniversario",
      "links": ["https:\/\/www.rumratings.com\/brands\/1246-barcelo-imperial-premium-blend-30-aniversario"]
    }, {
      "name": "Saint James XO",
      "links": ["http:\/\/www.saintjames-rum.com\/"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 15,
  "name": "Radek",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [255]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [256, 257, 258]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [255]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [259]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": []
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [260]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": []
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [256]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Brittish Navy Pusser\u00b4s Rum 75%",
      "links": ["http:\/\/whisky.nethar.cz\/forum\/viewtopic.php?f=29&t=1834"]
    }, {
      "name": "Brittish Navy Pusser\u00b4s Rum 54,5%",
      "links": ["https:\/\/whisky.nethar.cz\/forum\/viewtopic.php?f=29&t=1836"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 16,
  "name": "Nethar",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [7]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [261, 262, 263]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [264, 265, 37]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [7, 266, 267]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [268, 269, 270]
  }, {
    "categoryName": "Blended whisky",
    "items": [67, 46, 271]
  }, {
    "categoryName": "Blended malt",
    "items": [282, 272, 16]
  }, {
    "categoryName": "Grain whisky",
    "items": [274, 275, 176]
  }, {
    "categoryName": "USA a Kanada",
    "items": [276, 277, 131]
  }, {
    "categoryName": "Irsko",
    "items": []
  }, {
    "categoryName": "Japonsko",
    "items": [278, 279]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [73, 50, 280]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [281, {
      "name": "Akce na Drankdozijn",
      "links": ["https:\/\/drankdozijn.nl\/"]
    }, {
      "name": "SMWS z\u00e1vislost",
      "links": ["https:\/\/www.smws.com\/"]
    }]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [{
      "name": "Odlo\u017een\u00ed degustace Bratrstva ra\u0161eliny"
    }, {
      "name": "Plantation St. Lucia 2003",
      "links": ["https:\/\/www.rumratings.com\/brands\/1847-plantation-st-lucia-2003"]
    }, {
      "name": "V\u00fdpis z \u00fa\u010dtu po n\u00e1kupech whisky"
    }]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [129, 282, 266]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Malt Whisky Yearbook 2017",
      "links": ["https:\/\/www.goodreads.com\/book\/show\/31425823-malt-whisky-yearbook-2017"]
    }, {
      "name": "Malt Whisky Yearbook 2018",
      "links": ["https:\/\/www.goodreads.com\/book\/show\/36495523-malt-whisky-yearbook-2018"]
    }, {
      "name": "Ian Buxton: 101 Whiskies to Try Before You Die",
      "links": ["https:\/\/www.goodreads.com\/book\/show\/9313184-101-whiskies-to-try-before-you-die"]
    }]
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "Male\u0161ick\u00fd Origin\u00e1l N\u00b01 Pale ALE 11\u00b0",
      "links": ["http:\/\/www.malesickymikropivovar.cz"]
    }, {
      "name": "Rodinn\u00fd Pivovar 713 - Femme Fatale Imperial IPA",
      "links": ["http:\/\/www.hradeckepivo.cz\/"]
    }, {
      "name": "Krkono\u0161sk\u00fd medv\u011bd HopBit",
      "links": ["http:\/\/www.pivovarskabasta.cz"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "name": "BrewDog Jack Hammer",
      "links": ["https:\/\/www.brewdog.com\/"]
    }]
  }]
}, {
  "id": 17,
  "name": "xenopus",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [283, 284, 285]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [286, 287, 288]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [64, 289, 290]
  }, {
    "categoryName": "Blended whisky",
    "items": [194, 46]
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": [291]
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [null, 292]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [293, 50, 294]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": []
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [295]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": []
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 18,
  "name": "evening",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [37, 55, 296]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": []
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [297, 298, 299]
  }, {
    "categoryName": "Blended whisky",
    "items": [300, 301, 103]
  }, {
    "categoryName": "Blended malt",
    "items": []
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": [302, {
      "name": "",
      "links": ["https:\/\/www.masterofmalt.com\/spirit\/balcones\/balcones-rumble-whisky\/?srh=1"]
    }, 90]
  }, {
    "categoryName": "Irsko",
    "items": [303, 304]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50, 51, 305]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [50]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [306]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [304]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Rhum J.M Tr\u00e9s Vieux Agricole XO",
      "links": ["http:\/\/www.rhum-jm.com\/en\/collection\/tres-vieux-rhum-agricole-x-o"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "id": null,
      "name": "Plze\u0148sk\u00fd Prazdroj",
      "description": "",
      "links": ["http:\/\/www.pilsner-urquell.cz\/cz"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": [{
      "id": null,
      "name": "Plze\u0148sk\u00fd Prazdroj",
      "description": "",
      "links": ["http:\/\/www.pilsner-urquell.cz\/cz"]
    }]
  }]
}, {
  "id": 19,
  "name": "zombice",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [140]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [307, 308, 36]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [281, 309, 310]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [223, 311, 312]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [140, 33, 313]
  }, {
    "categoryName": "Blended whisky",
    "items": []
  }, {
    "categoryName": "Blended malt",
    "items": [48, 174, 16]
  }, {
    "categoryName": "Grain whisky",
    "items": []
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [314]
  }, {
    "categoryName": "Japonsko",
    "items": []
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [27, 315, 343]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [{
      "name": "Pal\u00edrna Tomatin a jejich \"nov\u011b\" p\u0159ed\u011blan\u00e1 \u0159ada lahv\u00ed"
    }, 173, 153]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [{
      "id": null,
      "name": "Pal\u00edrna Dufftown (a jejich \u0159ada Singleton)",
      "description": "",
      "links": []
    }, 316, 156]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [317, 318, 319]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": []
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 20,
  "name": "jos\u00e9",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [75]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [320, 321, 57]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [322, 284, 323]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [324, 325, 326]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [327, 269, 313]
  }, {
    "categoryName": "Blended whisky",
    "items": [67, 46, 328]
  }, {
    "categoryName": "Blended malt",
    "items": [329, 48, 49]
  }, {
    "categoryName": "Grain whisky",
    "items": [291, 330, 331]
  }, {
    "categoryName": "USA a Kanada",
    "items": [332, 333, 179]
  }, {
    "categoryName": "Irsko",
    "items": [249, 292, 93]
  }, {
    "categoryName": "Japonsko",
    "items": [null, 278]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50, 334, 335]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [{
      "name": "\u00faU\u010dast na n\u011bmeck\u00fdch whiskyfestivalech"
    }, {
      "name": "Kvalita SC rum\u016f Plantation"
    }]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [{
      "name": "Eshop Laphroaig v dob\u011b CS 9 a Cairdeas 2017"
    }]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [158, 323, 271]
  }, {
    "categoryName": "Kniha",
    "items": []
  }, {
    "categoryName": "Rum",
    "items": [{
      "id": null,
      "name": "Plantation SC Multi-Island Banyuls Cask Finish",
      "description": "",
      "links": ["https:\/\/www.rumratings.com\/brands\/2349-plantation-multi-island"]
    }, {
      "name": "Plantation SC Haiti XO White Pineau Finish",
      "links": ["https:\/\/www.rumratings.com\/brands\/4209-plantation-haiti-xo-single-cask-white-pineau-finish"]
    }, {
      "name": "Plantation SC Belize XO Grapia Finish",
      "links": ["https:\/\/www.rumratings.com\/brands\/4104-plantation-belize-xo-8-years-single-cask-grapia-finish"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": []
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}, {
  "id": 21,
  "name": "Fileta",
  "results": [{
    "categoryName": "Whisky roku\u00a0\u2013 absolutn\u00ed v\u00edt\u011bz",
    "items": [50]
  }, {
    "categoryName": "Skotsko single malt 9 a m\u00e9n\u011b let",
    "items": [80]
  }, {
    "categoryName": "Skotsko single malt 10 a\u017e 16 let",
    "items": [336]
  }, {
    "categoryName": "Skotsko single malt 17 a v\u00edce let",
    "items": [122]
  }, {
    "categoryName": "Skotsko single malt bez ud\u00e1n\u00ed st\u00e1\u0159\u00ed",
    "items": [337]
  }, {
    "categoryName": "Blended whisky",
    "items": [149]
  }, {
    "categoryName": "Blended malt",
    "items": [338]
  }, {
    "categoryName": "Grain whisky",
    "items": [135]
  }, {
    "categoryName": "USA a Kanada",
    "items": []
  }, {
    "categoryName": "Irsko",
    "items": [339]
  }, {
    "categoryName": "Japonsko",
    "items": [340]
  }, {
    "categoryName": "Zbytek sv\u011bta",
    "items": [50]
  }, {
    "categoryName": "P\u0159ekvapen\u00ed roku",
    "items": [341]
  }, {
    "categoryName": "Zklam\u00e1n\u00ed\/propad\u00e1k roku",
    "items": [103]
  }, {
    "categoryName": "Pom\u011br cena\/v\u00fdkon",
    "items": [342]
  }, {
    "categoryName": "Kniha",
    "items": [{
      "name": "Charles MacLean - Sv\u011bt whisky, 2. vyd\u00e1n\u00ed",
      "links": ["https:\/\/www.knizniklub.cz\/knihy\/236230-svet-whisky.html"]
    }]
  }, {
    "categoryName": "Rum",
    "items": [{
      "name": "Presidente Marti 15 yo",
      "links": ["http:\/\/www.presidente.cz\/nase-rumy"]
    }]
  }, {
    "categoryName": "Tuzemsk\u00e9 pivo",
    "items": [{
      "name": "pivovar Jadrn\u00ed\u010dek, N\u00e1m\u011b\u0161\u0165\u00e1k 12%",
      "links": ["http:\/\/pivovarjadrnicek.cz\/"]
    }]
  }, {
    "categoryName": "Zahrani\u010dn\u00ed pivo",
    "items": []
  }]
}];
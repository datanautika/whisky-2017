'use strict';

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

global.Promise = _bluebird2.default; /* eslint-disable no-self-compare, no-extend-native */

function includes(searchElement) {
	let fromIndex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

	if (this === null) {
		throw new TypeError('Array.prototype.includes called on null or undefined');
	}

	let O = Object(this);
	let len = parseInt(O.length, 10) || 0;

	if (len === 0) {
		return false;
	}

	let n = parseInt(fromIndex, 10);
	let k;

	if (n >= 0) {
		k = n;
	} else {
		k = len + n;

		if (k < 0) {
			k = 0;
		}
	}

	let currentElement;

	while (k < len) {
		currentElement = O[k];
		if (searchElement === currentElement || searchElement !== searchElement && currentElement !== currentElement) {
			// NaN !== NaN
			return true;
		}
		k++;
	}
	return false;
}

if (!Array.prototype.includes) {
	Array.prototype.includes = includes;
}

if (!String.prototype.startsWith) {
	String.prototype.startsWith = function (searchString, position) {
		return this.substr(position || 0, searchString.length) === searchString;
	};
}

function areIntlLocalesSupported() /*locales*/{
	let locales = arguments[0];

	if (typeof Intl === 'undefined') {
		return false;
	}

	if (!locales) {
		throw new Error('locales must be supplied.');
	}

	if (!Array.isArray(locales)) {
		locales = [locales];
	}

	let intlConstructors = [Intl.Collator, Intl.DateTimeFormat, Intl.NumberFormat].filter(intlConstructor => intlConstructor);

	if (intlConstructors.length === 0) {
		return false;
	}

	return intlConstructors.every(intlConstructor => {
		let supportedLocales = intlConstructor.supportedLocalesOf(locales);

		return supportedLocales.length === locales.length;
	});
}

if (global.Intl) {
	// Determine if the built-in `Intl` has the locale data we need.
	if (!areIntlLocalesSupported('cs-CZ')) {
		// `Intl` exists, but it doesn't have the data we need, so load the
		// polyfill and patch the constructors we need with the polyfill's.
		let IntlPolyfill = require('intl');

		Intl.NumberFormat = IntlPolyfill.NumberFormat;
		Intl.DateTimeFormat = IntlPolyfill.DateTimeFormat;
	}
} else {
	// No `Intl`, so use and load the polyfill.
	global.Intl = require('intl');
}
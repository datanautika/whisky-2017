'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _constants = require('../internals/constants');

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CLIENT_PLATFORM = _constants2.default.CLIENT_PLATFORM;
var SERVER_PLATFORM = _constants2.default.SERVER_PLATFORM;

/**
 * An object environment feature flags.
 */
var support = {};

/**
 * Returns string based on the platform (ie. server or client) ash is running on.
 */
support.platform = typeof exports !== 'undefined' && typeof global.process === 'object' ? SERVER_PLATFORM : CLIENT_PLATFORM;

exports.default = support;
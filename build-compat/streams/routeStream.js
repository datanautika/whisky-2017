'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _ayu = require('ayu');

var _ayu2 = _interopRequireDefault(_ayu);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _router = require('./router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ROUTE_STRIPPER = /^[#\/]|\s+$/g;

var appRoot = '/whiskyRoku2017'.replace(ROUTE_STRIPPER, '');

if (appRoot.length) {
	appRoot += '(/)';
}

var routeStream = _router2.default.add(appRoot + '(:page)(/:subpage)(/:sort)(/)').map(function (value) {
	var page = value.page,
	    subpage = value.subpage,
	    sort = value.sort;

	var group = null;

	if (history.state && typeof history.state.scroll !== 'undefined') {
		requestAnimationFrame(function () {
			window.scrollTo(0, history.state.scroll);
		});
	}

	if (page !== 'oblasti' && page !== 'uzivatele' && page !== 'kategorie' && page !== 'whisky') {
		page = null;
		subpage = null;
		sort = null;
	}

	if (_ayu2.default.isFiniteLike(subpage)) {
		subpage = parseInt(subpage, 10);
	} else if (page === 'whisky') {
		group = subpage;
		subpage = null;
	}

	return { page: page, subpage: subpage, group: group, sort: sort };
});

exports.default = routeStream;
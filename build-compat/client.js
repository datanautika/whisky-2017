'use strict';

require('./internals/polyfills');

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _App = require('./components/App');

var _App2 = _interopRequireDefault(_App);

var _Stream = require('./libs/Stream');

var _Stream2 = _interopRequireDefault(_Stream);

var _router = require('./streams/router');

var _router2 = _interopRequireDefault(_router);

var _BrowserRouter = require('./libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _I18n = require('./libs/I18n');

var _I18n2 = _interopRequireDefault(_I18n);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('./internals/constants');

var _constants2 = _interopRequireDefault(_constants);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

global.React = _react2.default;
global.$ = _jquery2.default;
global.Stream = _Stream2.default;
global.moment = _moment2.default;
global._ = _lodash2.default;

var G_KEY_CODE = 71;
var EN = _constants2.default.EN;

(0, _jquery2.default)('html').removeClass('noJavascript');

// grid toggle
(0, _jquery2.default)(document).on('keydown', function (event) {
	var tagName = event.target.tagName.toLowerCase();

	if (event.keyCode === G_KEY_CODE && event.target && tagName !== 'textarea' && tagName !== 'input') {
		(0, _jquery2.default)('body').toggleClass('hasGrid');
	}
});

// init i18n
_moment2.default.locale('cs');

var i18n = new _I18n2.default();

// default i18n settings, must be applied before creating router!
i18n.use({
	strings: _config2.default.i18nStrings,
	locale: EN,
	currency: '$'
});

// init router
var browserRouter = new _BrowserRouter2.default(_router2.default);

browserRouter.start();

// render app
var rootNode = document.getElementById('app');

if (rootNode) {
	_reactDom2.default.render(_react2.default.createElement(_App2.default, null), rootNode);
}

console.log('Oj!');
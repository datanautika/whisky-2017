'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.votesByCategory = exports.votes = exports.categories = exports.whiskies = exports.users = undefined;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _ayu = require('ayu');

var _ayu2 = _interopRequireDefault(_ayu);

var _computeCosineSimilarity = require('compute-cosine-similarity');

var _computeCosineSimilarity2 = _interopRequireDefault(_computeCosineSimilarity);

var _computeEuclideanDistance = require('compute-euclidean-distance');

var _computeEuclideanDistance2 = _interopRequireDefault(_computeEuclideanDistance);

var _users = require('./users');

var _whiskies = require('./whiskies');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable quote-props, quotes, indent, no-magic-numbers */

exports.users = _users.users;
exports.whiskies = _whiskies.whiskies;
var categories = exports.categories = [{
	"id": 1,
	"group": 1,
	"name": "Whisky roku – absolutní vítěz",
	"points": [3],
	"hasWhiskies": true
}, {
	"id": 2,
	"group": 1,
	"name": "Skotsko single malt 9 a méně let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 3,
	"group": 1,
	"name": "Skotsko single malt 10 až 16 let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 4,
	"group": 1,
	"name": "Skotsko single malt 17 a více let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 5,
	"group": 1,
	"name": "Skotsko single malt bez udání stáří",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 6,
	"group": 1,
	"name": "Blended whisky",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 7,
	"group": 1,
	"name": "Blended malt",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 8,
	"group": 1,
	"name": "Grain whisky",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 9,
	"group": 2,
	"name": "USA a Kanada",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 10,
	"group": 2,
	"name": "Irsko",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 11,
	"group": 2,
	"name": "Japonsko",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 12,
	"group": 2,
	"name": "Zbytek světa",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 13,
	"group": 3,
	"name": "Překvapení roku",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 14,
	"group": 4,
	"name": "Zklamání/propadák roku",
	"points": [],
	"hasWhiskies": true
}, {
	"id": 15,
	"group": 5,
	"name": "Poměr cena/výkon",
	"points": [],
	"hasWhiskies": true
}, {
	"id": 16,
	"group": 6,
	"name": "Kniha",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 17,
	"group": 6,
	"name": "Rum",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 18,
	"group": 6,
	"name": "Tuzemské pivo",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 19,
	"group": 6,
	"name": "Zahraniční pivo",
	"points": [],
	"hasWhiskies": false
}];

var votes = exports.votes = [];

var voteId = 0;

_users.users.forEach(function (user) {
	user.whiskiesVector = _lodash2.default.times(_whiskies.whiskies.length, _lodash2.default.constant(0));
	user.similarities = _lodash2.default.times(_users.users.length, _lodash2.default.constant(0));
});

_users.users.forEach(function (user) {
	return user.results.forEach(function (result) {
		return result.items.forEach(function (item, index) {
			var id = void 0;

			if (item && typeof item === 'number') {
				id = +item;
			} else if (item && typeof item === 'object' && typeof item.id === 'number') {
				id = +item.id;
			} else {
				return;
			}

			var category = _lodash2.default.find(categories, { name: result.categoryName });

			if (!category) {
				return;
			}

			var vote = _lodash2.default.cloneDeep(_lodash2.default.find(_whiskies.whiskies, { id: id }));

			if (!vote) {
				return;
			}

			vote.whiskyId = vote.id;
			vote.id = ++voteId;
			vote.userId = user.id;
			vote.categoryName = category.name;
			vote.categoryGroup = category.group;
			vote.pointsCount = category.points && category.points[index] ? category.points[index] : 0;

			if (vote.pointsCount) {
				// user.whiskiesVector[vote.whiskyId - 1] = 1;
			}
			user.whiskiesVector[vote.whiskyId - 1] += vote.pointsCount;

			votes.push(vote);
		});
	});
});

_users.users.forEach(function (user1) {
	_users.users.forEach(function (user2, user2Index) {
		user1.similarities[user2Index] = {
			userId: user2.id,
			userName: user2.name,
			cosineSimilarity: (0, _computeCosineSimilarity2.default)(user1.whiskiesVector, user2.whiskiesVector),
			euclideanDistance: (0, _computeEuclideanDistance2.default)(user1.whiskiesVector, user2.whiskiesVector)
		};
	});

	user1.similarities.sort(function (similarity1, similarity2) {
		return similarity2.cosineSimilarity - similarity1.cosineSimilarity;
	});
});

global.whiskies = _whiskies.whiskies;
global.users = _users.users;
global.cosineSimilarity = _computeCosineSimilarity2.default;

var votesByCategory = exports.votesByCategory = new _ayu2.default.DataModel({
	whiskyIds: new _ayu2.default.Dimension({
		variable: 'whiskyId',
		scale: _ayu2.default.NOMINAL_SCALE
	}),
	voteIds: new _ayu2.default.Dimension({
		variable: 'id',
		scale: _ayu2.default.NOMINAL_SCALE
	}),
	categoryName: new _ayu2.default.Dimension({
		variable: 'categoryName',
		scale: _ayu2.default.NOMINAL_SCALE,
		group: true
	})
}, votes);
'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Header = require('./Header.css');

var _Header2 = _interopRequireDefault(_Header);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

var _BrowserRouter = require('../libs/BrowserRouter');

var _BrowserRouter2 = _interopRequireDefault(_BrowserRouter);

var _router = require('../streams/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

var isUrlExternal = function isUrlExternal(url) {
	var testedDomain = DOMAIN_REGEX.exec(url);
	var localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

var browserRouter = new _BrowserRouter2.default(_router2.default);

var Header = function (_React$PureComponent) {
	(0, _inherits3.default)(Header, _React$PureComponent);

	function Header() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, Header);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Header.__proto__ || (0, _getPrototypeOf2.default)(Header)).call.apply(_ref, [this].concat(args))), _this), _this.handleClick = function (event) {
			if (event.button !== 1) {
				var url = event.target.getAttribute('href');

				if (url && !DOMAIN_REGEX.test(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				} else if (url && !isUrlExternal(url)) {
					event.preventDefault();

					browserRouter.navigate(url);
				}
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(Header, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: _Header2.default.root, onClick: this.handleClick },
				_react2.default.createElement(
					'h1',
					null,
					_react2.default.createElement(
						'a',
						{ href: (0, _href2.default)('') },
						'Whisky roku 2017'
					)
				)
			);
		}
	}]);
	return Header;
}(_react2.default.PureComponent);

exports.default = Header;
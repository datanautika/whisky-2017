'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Footer = require('./Footer.css');

var _Footer2 = _interopRequireDefault(_Footer);

var _href = require('../utils/href');

var _href2 = _interopRequireDefault(_href);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Footer = function (_React$PureComponent) {
	(0, _inherits3.default)(Footer, _React$PureComponent);

	function Footer() {
		(0, _classCallCheck3.default)(this, Footer);
		return (0, _possibleConstructorReturn3.default)(this, (Footer.__proto__ || (0, _getPrototypeOf2.default)(Footer)).apply(this, arguments));
	}

	(0, _createClass3.default)(Footer, [{
		key: 'render',
		value: function render() {
			return _react2.default.createElement(
				'div',
				{ className: _Footer2.default.root },
				_react2.default.createElement(
					'ol',
					null,
					_react2.default.createElement(
						'li',
						null,
						'v2.0.1'
					),
					_react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: 'https://whisky.nethar.cz/forum/viewtopic.php?f=6&t=2889' },
							'Pravidla'
						)
					),
					_react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: 'http://whisky.nethar.cz/forum/' },
							'F\xF3rum'
						)
					),
					_react2.default.createElement(
						'li',
						null,
						_react2.default.createElement(
							'a',
							{ href: (0, _href2.default)('dist', 'data.zip') },
							'St\xE1hnout data'
						)
					)
				),
				_react2.default.createElement(
					'p',
					{ className: _Footer2.default.logo },
					_react2.default.createElement(
						'a',
						{ href: 'http://www.datanautika.com' },
						_react2.default.createElement('img', { src: (0, _href2.default)('assets', 'datanautika.svg'), alt: 'Made by Datanautika' })
					)
				)
			);
		}
	}]);
	return Footer;
}(_react2.default.PureComponent);

exports.default = Footer;
'use strict';

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

require('./internals/polyfills');

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _koaCompress = require('koa-compress');

var _koaCompress2 = _interopRequireDefault(_koaCompress);

var _koaLogger = require('koa-logger');

var _koaLogger2 = _interopRequireDefault(_koaLogger);

var _koaBodyparser = require('koa-bodyparser');

var _koaBodyparser2 = _interopRequireDefault(_koaBodyparser);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _koaStatic = require('koa-static');

var _koaStatic2 = _interopRequireDefault(_koaStatic);

var _appRoot = require('./internals/appRoot');

var _appRoot2 = _interopRequireDefault(_appRoot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var HTTP_NOT_FOUND = 404;
var HTTP_INTERNAL_ERROR = 500;
var ROUTE_STRIPPER = /^[#\/]|\s+$/g;

var readFile = _promise2.default.promisify(_fs2.default.readFile);
var app = new _koa2.default();
var uriAppRoot = '/whiskyRoku2017'.replace(ROUTE_STRIPPER, '');

console.log('App root dir:', '"' + _appRoot2.default + '"'); // eslint-disable-line no-console
console.log('App root URL:', '"' + uriAppRoot + '"'); // eslint-disable-line no-console

app.use((0, _koaBodyparser2.default)());
app.use((0, _koaLogger2.default)());
app.use(function () {
	var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(context, next) {
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						_context.prev = 0;
						_context.next = 3;
						return next();

					case 3:
						_context.next = 11;
						break;

					case 5:
						_context.prev = 5;
						_context.t0 = _context['catch'](0);

						console.warn('\nError!\n', _context.t0); // eslint-disable-line no-console

						context.status = _context.t0.status || HTTP_INTERNAL_ERROR;

						context.body = {
							error: {
								message: _context.t0.message
							}
						};

						context.app.emit('error', _context.t0, context);

					case 11:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, undefined, [[0, 5]]);
	}));

	return function (_x, _x2) {
		return _ref.apply(this, arguments);
	};
}());
app.use((0, _koaStatic2.default)(_path2.default.resolve(_path2.default.join(_appRoot2.default, 'public'))));

// serve app
app.use(function () {
	var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(context, next) {
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						if (!(context.method !== 'HEAD' && context.method !== 'GET')) {
							_context2.next = 4;
							break;
						}

						_context2.next = 3;
						return next();

					case 3:
						return _context2.abrupt('return');

					case 4:
						if (!(context.body && context.body !== null || context.status !== HTTP_NOT_FOUND)) {
							_context2.next = 8;
							break;
						}

						_context2.next = 7;
						return next();

					case 7:
						return _context2.abrupt('return');

					case 8:
						_context2.next = 10;
						return readFile(_path2.default.join(_appRoot2.default, 'public/index.html'), 'utf8');

					case 10:
						context.body = _context2.sent;
						_context2.next = 13;
						return next();

					case 13:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, undefined);
	}));

	return function (_x3, _x4) {
		return _ref2.apply(this, arguments);
	};
}());

// compression
app.use((0, _koaCompress2.default)());

var PORT = 8080;

_http2.default.createServer(app.callback()).listen(undefined || PORT);

console.log('Listening on port ' + (undefined || PORT) + '...'); // eslint-disable-line no-console
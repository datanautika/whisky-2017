'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = href;
var ROUTE_STRIPPER = /^[#\/]|\s+$/g;

var appRoot = '/whiskyRoku2017'.replace(ROUTE_STRIPPER, '');

function href() {
	for (var _len = arguments.length, levels = Array(_len), _key = 0; _key < _len; _key++) {
		levels[_key] = arguments[_key];
	}

	return (appRoot ? '/' + appRoot : '') + '/' + levels.join('/');
}
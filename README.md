Whisky of the year 2017
=======================


## Installation

- Install **[Node.js](http://nodejs.org/) v7**.
- Install **[Git](https://git-scm.com/) v2.9.x**
- Clone the repo and run from **PowerShell**:
    - `./git-setup` (**This is super important!**)
    - `npm install`
    - `npm install gulpjs/gulp#4.0 -g`
    - `npm install node-dev -g`
    - `npm run production-build`


## Run

- Dev server: `npm start`
- Visit [http://localhost:8080](http://localhost:8080)


## License

Copyright 2017 Datanautika s.r.o.

[The MIT License](https://github.com/datanautika/ash/blob/master/LICENSE)

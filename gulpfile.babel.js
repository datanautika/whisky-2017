/* eslint-disable no-sync, no-console */

import './src/internals/cssModules';

import path from 'path';
import gulp from 'gulp';
import del from 'del';
import babel from 'gulp-babel';
import fs from 'fs';
import ExtractText from 'extract-text-webpack-plugin';
import autoprefixer from 'autoprefixer';
import postcssVariables from 'postcss-css-variables';
import postcssVerticalRhythm from 'postcss-vertical-rhythm';
import postcssNested from 'postcss-nested';
import postcssPxToRem from 'postcss-pxtorem';
import postcssCalc from 'postcss-calc';
import postcssConditionals from 'postcss-conditionals';
import postcssCustomMedia from 'postcss-custom-media';
import util from 'gulp-util';
import uglify from 'gulp-uglify';
import cssnano from 'gulp-cssnano';
import gulpDebug from 'gulp-debug';
import chalk from 'chalk';
import replace from 'gulp-replace';
import zip from 'gulp-zip';
import webpack from 'webpack';

import flattenTree from './src/internals/flattenTree';
import stylesConfig from './src/config/styles';


if (util.env.production) {
	console.log('Production environment!'); // eslint-disable-line no-console

	process.env.NODE_ENV = 'production';
	process.env.APP_ROOT = '/whiskyRoku2017'; // must be with leading /, e.g. /appRoot
} else {
	console.log('Development environment!'); // eslint-disable-line no-console

	process.env.NODE_ENV = 'development';
	process.env.APP_ROOT = '/';
}

const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

let appRoot = process.env.APP_ROOT.replace(ROUTE_STRIPPER, '');

if (appRoot.length) {
	appRoot = `/${appRoot}`;
}

let variables = flattenTree(stylesConfig, {valuesToString: true});

// console.log(variables);

const PATHS = {
	nextBuildDir: './build-next/',
	compatBuildDir: './build-compat/',
	publicAssetsDir: './public/assets',
	publicDistDir: './public/dist',
	publicDir: './public',

	srcFiles: ['./src/**/*.js'],
	nextBuildFiles: './build-next/**/*',
	compatBuildFiles: './build-compat/**/*',
	clientBuildFile: './build-compat/client.js',
	clientScriptDistFile: './public/dist/client.js',
	clientStylesDistFile: './public/dist/client.css',
	imageFiles: './assets/images/**/*',
	modelFiles: './assets/models/**/*',
	fontFiles: './assets/fonts/**/*.woff*',
	publicFiles: './public/**/*',
	stylesFiles: './src/**/*.css',
	dataFiles: './assets/data/**/*',
	templateFiles: ['./assets/templates/**/*', './assets/templates/**/.htaccess']
};

const TASKS = {
	watch: 'watch',
	build: 'build',
	cleanup: 'cleanup',
	nextBuild: 'next-build',
	compatBuild: 'compat-build',
	dist: 'dist',
	styles: 'styles',
	fonts: 'fonts',
	images: 'images',
	templates: 'templates',
	data: 'data',
	minifyScripts: 'minifiy-scripts',
	minifyStyles: 'minify-styles'
};

let mediaQueries = {
	'--tinyMenu-start-min': `(min-width: ${variables['breakpoints.tinyMenu.start']}px)`,
	'--tinyMenu-start-max': `(max-width: ${variables['breakpoints.tinyMenu.start']}px)`,
	'--tinyMenu-end-min': `(min-width: ${variables['breakpoints.tinyMenu.end']}px)`,
	'--tinyMenu-end-max': `(max-width: ${variables['breakpoints.tinyMenu.end']}px)`,
	'--compactMenu-start-min': `(min-width: ${variables['breakpoints.compactMenu.start']}px)`,
	'--compactMenu-start-max': `(max-width: ${variables['breakpoints.compactMenu.start']}px)`,
	'--compactMenu-end-min': `(min-width: ${variables['breakpoints.compactMenu.end']}px)`,
	'--compactMenu-end-max': `(max-width: ${variables['breakpoints.compactMenu.end']}px)`,
	'--compactPage-start-min': `(min-width: ${variables['breakpoints.compactPage.start']}px)`,
	'--compactPage-start-max': `(max-width: ${variables['breakpoints.compactPage.start']}px)`,
	'--compactPage-end-min': `(min-width: ${variables['breakpoints.compactPage.end']}px)`,
	'--compactPage-end-max': `(max-width: ${variables['breakpoints.compactPage.end']}px)`,
	'--singleColumnPage-start-min': `(min-width: ${variables['breakpoints.singleColumnPage.start']}px)`,
	'--singleColumnPage-start-max': `(max-width: ${variables['breakpoints.singleColumnPage.start']}px)`,
	'--singleColumnPage-middle-min': `(min-width: ${variables['breakpoints.singleColumnPage.middle']}px)`,
	'--singleColumnPage-middle-max': `(max-width: ${variables['breakpoints.singleColumnPage.middle']}px)`,
	'--singleColumnPage-end-min': `(min-width: ${variables['breakpoints.singleColumnPage.end']}px)`,
	'--singleColumnPage-end-max': `(max-width: ${variables['breakpoints.singleColumnPage.end']}px)`,
};


/**
 * Cleanup task
 *
 * Cleans all files created by various build tasks.
 */
gulp.task(TASKS.cleanup, () => del([PATHS.publicFiles, PATHS.nextBuildDir, PATHS.compatBuildDir]));


/**
 * Build scripts, next JS version
 *
 * Build JavaScript files for use in Node.js.
 */
let babelNextConfig = JSON.parse(fs.readFileSync('.babelrc-next', {encoding: 'utf8'}));

gulp.task(TASKS.nextBuild, (done) => gulp.src(PATHS.srcFiles, {since: gulp.lastRun(TASKS.nextBuild)})
	// .pipe(gulpDebug({title: TASKS.nextBuild}))
	.pipe(babel(babelNextConfig).on('error', (error) => {
		console.log(`${chalk.red(`${error.fileName}${(error.loc ?	`(${error.loc.line}, ${error.loc.column}): ` :	': ')}`)}error Babel: ${error.message}\n${error.codeFrame}`);

		done();
	}))
	.pipe(gulp.dest(PATHS.nextBuildDir)));


/**
 * Build scripts, compat JS version
 *
 * Build JavaScript files for use in browsers.
 */
let babelCompatConfig = JSON.parse(fs.readFileSync('.babelrc-compat', {encoding: 'utf8'}));

gulp.task(TASKS.compatBuild, (done) => gulp.src(PATHS.srcFiles, {since: gulp.lastRun(TASKS.compatBuild)})
	// .pipe(gulpDebug({title: TASKS.compatBuild}))
	.pipe(babel(babelCompatConfig).on('error', (error) => {
		console.log(`${chalk.red(`${error.fileName}${(error.loc ?	`(${error.loc.line}, ${error.loc.column}): ` :	': ')}`)}error Babel: ${error.message}\n${error.codeFrame}`);

		done();
	}))
	.pipe(gulp.dest(PATHS.compatBuildDir)));


/**
 * Create dist files
 *
 * Create dist files of scripts and styles.
 */
let webpackCompiler = webpack({
	context: path.resolve(__dirname),
	entry: PATHS.clientBuildFile,
	output: {
		filename: path.basename(PATHS.clientScriptDistFile),
		path: path.resolve(__dirname, PATHS.publicDistDir)
	},
	cache: true,
	module: {
		loaders: [{
			test: /\.css$/,
			// loader: ExtractText.extract('style-loader', 'css-loader?module&importLoaders=1&localIdentName=[name]__[local]__[hash:base64:4]!postcss-loader')
			loader: ExtractText.extract('style-loader', 'css-loader?module&importLoaders=1&localIdentName=[name]__[local]&-url!postcss-loader')
		}]
	},
	plugins: [new ExtractText(path.basename(PATHS.clientStylesDistFile), {allChunks: true}), new webpack.DefinePlugin({
		'process.env': {
			NODE_ENV: JSON.stringify(process.env.NODE_ENV),
			APP_ROOT: JSON.stringify(process.env.APP_ROOT)
		}
	}), new webpack.optimize.DedupePlugin()],
	postcss: [
		postcssNested(),
		postcssVariables({variables}),
		postcssCustomMedia({extensions: mediaQueries}),
		postcssConditionals(),
		postcssVerticalRhythm({
			unit: 'bh',
			baselineHeight: stylesConfig.grid.baselineHeight
		}),
		postcssCalc({precision: 8}),
		postcssPxToRem({
			rootValue: stylesConfig.typographicScale.base,
			unitPrecision: 8,
			propWhiteList: ['font', 'font-size', 'line-height', 'letter-spacing', 'width', 'height', 'left', 'right', 'top', 'bottom', 'background-size', 'margin', 'margin-left', 'margin-right', 'margin-top', 'margin-bottom', 'padding', 'padding-left', 'padding-right', 'padding-top', 'padding-bottom', 'border', 'border-left', 'border-right', 'border-top', 'border-bottom', 'background', 'transform'],
			replace: true,
			mediaQuery: false,
			selectorBlackList: ['html']
		}),
		autoprefixer({
			browsers: ['> 1%', 'last 2 versions'],
			remove: false
		})
	],
	watch: false
});

gulp.task(TASKS.dist, (done) => {
	webpackCompiler.run((error, stats) => {
		if (error) {
			util.log('Error', error);

			if (done) {
				done();
			}
		} else {
			Object.keys(stats.compilation.assets).forEach((key) => {
				util.log('Webpack: output ', util.colors.green(key));
			});

			if (done) {
				done();
			}
		}
	});
});


/**
 * Copy styles
 *
 * Copy styles to all the scripts build directories.
 */
gulp.task(TASKS.styles, () => gulp.src(PATHS.stylesFiles, {since: gulp.lastRun(TASKS.styles)})
	.pipe(gulpDebug({title: TASKS.styles}))
	.pipe(replace('%{appRoot}', appRoot))
	.pipe(gulp.dest(PATHS.nextBuildDir))
	.pipe(gulp.dest(PATHS.compatBuildDir)));


/**
 * Copy fonts
 */
gulp.task(TASKS.fonts, () => gulp.src(PATHS.fontFiles)
	.pipe(gulp.dest(PATHS.publicAssetsDir)));


/**
 * Copy images
 */
gulp.task(TASKS.images, () => gulp.src(PATHS.imageFiles)
	.pipe(gulp.dest(PATHS.publicAssetsDir)));


/**
 * Copy templates
 */
gulp.task(TASKS.templates, () => gulp.src(PATHS.templateFiles)
	.pipe(gulpDebug({title: TASKS.templates}))
	.pipe(replace('%{appRoot}', appRoot))
	.pipe(gulp.dest(PATHS.publicDir)));


/**
 * Zip data files
 */
gulp.task(TASKS.data, () => gulp.src(PATHS.dataFiles)
	.pipe(gulpDebug({title: TASKS.data}))
	.pipe(zip('data.zip'))
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Minify dist script files
 */
gulp.task(TASKS.minifyScripts, () => gulp.src(PATHS.clientScriptDistFile)
	.pipe(uglify())
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Minify dist styles file
 */
gulp.task(TASKS.minifyStyles, () => gulp.src(PATHS.clientStylesDistFile)
	.pipe(cssnano({
		mergeLonghand: false,
		autoprefixer: false,
		safe: true
	}))
	.pipe(gulp.dest(PATHS.publicDistDir)));


/**
 * Watch task
 */
gulp.task(TASKS.watch, (done) => {
	gulp.watch([...PATHS.srcFiles], gulp.series(gulp.parallel(TASKS.nextBuild, TASKS.compatBuild), TASKS.dist));
	gulp.watch([PATHS.stylesFiles], gulp.series(TASKS.styles, TASKS.dist));
	gulp.watch([PATHS.imageFiles], gulp.series(TASKS.images));

	done();
});


/**
 * Default task
 *
 * Builds development build and starts watching for changes.
 */
gulp.task('default', gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.templates, TASKS.images, TASKS.fonts, TASKS.data, gulp.series(
			TASKS.styles,
			gulp.parallel(TASKS.nextBuild, TASKS.compatBuild),
			TASKS.dist
		)
	),
	TASKS.watch
));


/**
 * Build task
 */
gulp.task(TASKS.build, process.env.NODE_ENV === 'production' ? gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.templates, TASKS.images, TASKS.fonts, TASKS.data, gulp.series(
			TASKS.styles,
			gulp.parallel(TASKS.nextBuild, TASKS.compatBuild),
			TASKS.dist,
			gulp.parallel(TASKS.minifyScripts, TASKS.minifyStyles)
		)
	)
) : gulp.series(
	TASKS.cleanup,
	gulp.parallel(
		TASKS.templates, TASKS.images, TASKS.fonts, TASKS.data, gulp.series(
			TASKS.styles,
			gulp.parallel(TASKS.nextBuild, TASKS.compatBuild),
			TASKS.dist
		)
	)
));

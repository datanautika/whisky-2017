import React from 'react';
import _ from 'lodash';

import styles from './Category.css';
import WhiskyName from './WhiskyName';
import {whiskies, votes, votesByCategory, users} from '../data';
import whiskyName from '../utils/whiskyName';
import href from '../utils/href';


export default class Category extends React.Component {
	render() {
		let category = this.props.data;
		let categoryResults = _.find(votesByCategory.values, {groups: {categoryName: category.name}});
		let selectedWhiskies = [];
		let whiskiesElements;

		if (categoryResults) {
			categoryResults.whiskyIds.forEach((whiskyId, index) => {
				let whisky = _.find(selectedWhiskies, {whisky: {id: whiskyId}});

				if (whisky) {
					whisky.pointsCount += _.find(votes, {id: categoryResults.voteIds[index]}).pointsCount;
				} else {
					selectedWhiskies.push({
						whisky: _.find(whiskies, {id: whiskyId}),
						pointsCount: _.find(votes, {id: categoryResults.voteIds[index]}).pointsCount
					});
				}
			});

			whiskiesElements = selectedWhiskies.sort((a, b) => {
				if (category.id === 14) { // "Zklamání/propadák roku"
					let result = a.pointsCount - b.pointsCount;

					if (result !== 0) {
						return result;
					}

					return whiskyName(a.whisky).join(' ').trim().localeCompare(whiskyName(b.whisky).join(' ').trim());
				}

				let result = b.pointsCount - a.pointsCount;

				if (result !== 0) {
					return result;
				}

				return whiskyName(a.whisky).join(' ').trim().localeCompare(whiskyName(b.whisky).join(' ').trim());
			}).map((selectedWhisky, index) => <li key={index} className={styles.whisky}><WhiskyName data={selectedWhisky.whisky} />{selectedWhisky.pointsCount ? <span className={styles.pointsCount}>{`${selectedWhisky.pointsCount} `}<span className={styles.pointsCountLabel}>{((value) => {
				if (value === 1) {
					return 'bod';
				}

				if (value === 2 || value === 3 || value === 4) {
					return 'body';
				}

				return 'bodů';
			})(selectedWhisky.pointsCount)}</span></span> : null}</li>);
		}


		let selectedUsers = users.map((user) => ({
			name: user.name,
			result: user.results.filter((result) => result.categoryName === category.name)[0]
		})).sort((a, b) => a.name.trim().localeCompare(b.name.trim()));

		return <div className={styles.root}>
			<h2 className={styles.heading}>{category.name}</h2>
			{whiskiesElements && whiskiesElements.length ? <ol className={styles.whiskies}>
				{whiskiesElements}
			</ol> : null}

			<section>
				<h3 className={styles.votesHeading}>Umístění</h3>

				{selectedUsers.map((user, userIndex) => {
					let elements = user.result.items.map((item, itemIndex) => {
						if (item && typeof item === 'number') {
							return <li key={itemIndex} className={styles.whisky}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item})} /></li>;
						} else if (item && typeof item === 'object' && typeof item.id === 'number') {
							return <li key={itemIndex} className={styles.whisky}><WhiskyName key={itemIndex} data={_.find(whiskies, {id: item.id})} /></li>;
						} else if (item && typeof item === 'object' && (typeof item.name === 'string' || typeof item.description === 'string')) {
							return <li key={itemIndex} className={styles.categoryItem}>
								<p className={styles.categoryItemName}>{item.name}</p>
							</li>;
						}

						return <li key={itemIndex} className={`${styles.whisky} ${styles.isEmpty}`}><p>–</p></li>;
					});

					return elements.length ? <section key={userIndex} className={styles.result}>
						<header className={styles.resultHeader}>
							<h3 className={styles.resultHeading}>
								<span className={styles.resultHeadingLabel}>Uživatel</span>
								<a href={href('uzivatele', user.name)}>{user.name}</a>
							</h3>
						</header>

						<ol>{elements}</ol>
					</section> : null;
				})}
			</section>
		</div>;
	}
}

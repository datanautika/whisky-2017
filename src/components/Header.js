import React from 'react';

import styles from './Header.css';
import href from '../utils/href';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';


const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

export default class Header extends React.PureComponent {
	render() {
		return <div className={styles.root} onClick={this.handleClick}>
			<h1><a href={href('')}>Whisky roku 2017</a></h1>
		</div>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = event.target.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}

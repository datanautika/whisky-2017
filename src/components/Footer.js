import React from 'react';

import styles from './Footer.css';
import href from '../utils/href';


export default class Footer extends React.PureComponent {
	render() {
		return <div className={styles.root}>
			<ol>
				<li>v2.0.1</li>
				<li><a href="https://whisky.nethar.cz/forum/viewtopic.php?f=6&t=2889">Pravidla</a></li>
				<li><a href="http://whisky.nethar.cz/forum/">Fórum</a></li>
				<li><a href={href('dist', 'data.zip')}>Stáhnout data</a></li>
			</ol>
			<p className={styles.logo}>
				<a href="http://www.datanautika.com"><img src={href('assets', 'datanautika.svg')} alt="Made by Datanautika" /></a>
			</p>
		</div>;
	}
}

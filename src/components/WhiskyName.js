import React from 'react';

import styles from './WhiskyName.css';
import whiskyName from '../utils/whiskyName';
import href from '../utils/href';
import BrowserRouter from '../libs/BrowserRouter';
import router from '../streams/router';


const DOMAIN_REGEX = /https?:\/\/((?:[\w\d]+\.)+[\w\d]{2,})/i;

let isUrlExternal = (url) => {
	let testedDomain = DOMAIN_REGEX.exec(url);
	let localDomain = DOMAIN_REGEX.exec(location.href);

	if (!localDomain) {
		return true;
	}

	if (testedDomain && testedDomain[1]) {
		return testedDomain[1] !== localDomain[1];
	}

	return false;
};

let browserRouter = new BrowserRouter(router);

export default class WhiskyName extends React.Component {
	shouldComponentUpdate(newProps) {
		let shouldUpdate = !this.props.data.links && !newProps.data.links ||
			this.props.data.links && newProps.data.links && this.props.data.links[0] !== newProps.data.links[0] ||
			this.props.data.age !== newProps.data.age ||
			this.props.data.batch !== newProps.data.batch ||
			this.props.data.bottled !== newProps.data.bottled ||
			this.props.data.bottledFor !== newProps.data.bottledFor ||
			this.props.data.bottler !== newProps.data.bottler ||
			this.props.data.bottles !== newProps.data.bottles ||
			this.props.data.brand !== newProps.data.brand ||
			this.props.data.caskNumber !== newProps.data.caskNumber ||
			this.props.data.caskType !== newProps.data.caskType ||
			// this.props.data.categoryGroup !== newProps.data.categoryGroup ||
			// this.props.data.categoryName !== newProps.data.categoryName ||
			this.props.data.country !== newProps.data.country ||
			this.props.data.distillery !== newProps.data.distillery ||
			this.props.data.edition !== newProps.data.edition ||
			this.props.data.id !== newProps.data.id ||
			// this.props.data.image !== newProps.data.image ||
			this.props.data.isCaskStrength !== newProps.data.isCaskStrength ||
			this.props.data.isNonChillfiltered !== newProps.data.isNonChillfiltered ||
			this.props.data.isSingleCask !== newProps.data.isSingleCask ||
			this.props.data.isUncolored !== newProps.data.isUncolored ||
			this.props.data.name !== newProps.data.name ||
			// this.props.data.pointsCount !== newProps.data.pointsCount ||
			this.props.data.region !== newProps.data.region ||
			this.props.data.size !== newProps.data.size ||
			this.props.data.strength !== newProps.data.strength ||
			this.props.data.type !== newProps.data.type ||
			// this.props.data.userId !== newProps.data.userId ||
			this.props.data.vintage !== newProps.data.vintage;

		return shouldUpdate;
	}

	render() {
		let whisky = this.props.data;
		let isSingleMalt = whisky.type === 'Single Malt';

		if (!whisky) {
			return null;
		}

		let [titlePart1, titlePart2, titlePart3, subtitle] = whiskyName(whisky);
		let regionClass;

		if (whisky.region) {
			regionClass = styles[whisky.region.toLowerCase()];
		}

		return <a ref="root" className={styles.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${styles.isSingleMalt}` : '')} href={href('whisky', whisky.id)} onClick={this.handleClick}>
			<span className={styles.title}>{titlePart1 ? titlePart1 : null}{titlePart2 ? <b>{titlePart2}</b> : null}{titlePart3 ? titlePart3 : null}</span>
			<span className={styles.subtitle}>{subtitle ? subtitle : null}</span>
		</a>;
	}

	handleClick = (event) => {
		if (event.button !== 1) {
			let url = this.refs.root.getAttribute('href');

			if (url && !DOMAIN_REGEX.test(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			} else if (url && !isUrlExternal(url)) {
				event.preventDefault();

				browserRouter.navigate(url);
			}
		}
	};
}

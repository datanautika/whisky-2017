import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import styles from './WhiskyDetail.css';
import whiskyName from '../utils/whiskyName';
import href from '../utils/href';
import {users, categories} from '../data';


const NUMBER_FORMAT = new Intl.NumberFormat('cs-CZ', {minimumFractionDigits: 1, maximumFractionDigits: 1});
const ML_IN_L = 1000;

let name = (whisky) => {
	let [titlePart1, titlePart2, titlePart3, subtitle] = whiskyName(whisky);

	return <h2 className={styles.heading}>
		<span className={styles.title}>{titlePart1 ? titlePart1 : null}{titlePart2 ? <b>{titlePart2}</b> : null}{titlePart3 ? titlePart3 : null}</span>
		<span className={styles.subtitle}>{subtitle ? subtitle : null}</span>
	</h2>;
};

let parseDate = (dateString) => {
	let partsCount = dateString.match(/-/g);

	if (partsCount <= 1) {
		return dateString;
	}

	let date = moment(dateString);

	if (partsCount > 1) {
		return date.format('Do MMMM YYYY');
	}

	return date.format('MMMM YYYY');
};

export default class WhiskyDetail extends React.Component {
	render() {
		let whisky = this.props.data;
		let regionClass;
		let regionName;
		let countryName = whisky.country;
		let isSingleMalt = whisky.type === 'Single Malt';

		if (whisky.region) {
			regionClass = styles[whisky.region.toLowerCase()];

			if (whisky.region === 'Islay') {
				regionName = 'Islay';
			} else if (whisky.region === 'Islands') {
				regionName = 'Ostrovy';
			} else if (whisky.region === 'Highlands') {
				regionName = 'Vysočina';
			} else if (whisky.region === 'Lowlands') {
				regionName = 'Nížina';
			} else if (whisky.region === 'Speyside') {
				regionName = 'Speyside';
			} else if (whisky.region === 'Campbeltown') {
				regionName = 'Campbeltown';
			}
		}

		if (whisky.country === 'Scotland') {
			countryName = 'Skotsko';
		}

		if (whisky.country === 'Ireland') {
			countryName = 'Irsko';
		}

		if (whisky.country === 'United States') {
			countryName = 'USA';
		}

		if (whisky.country === 'France') {
			countryName = 'Francie';
		}

		if (whisky.country === 'India') {
			countryName = 'Indie';
		}

		if (whisky.country === 'Canada') {
			countryName = 'Kanada';
		}

		if (whisky.country === 'Czech Republic') {
			countryName = 'Česko';
		}

		if (whisky.country === 'Sweden') {
			countryName = 'Švédsko';
		}

		if (whisky.country === 'Australia') {
			countryName = 'Austrálie';
		}

		if (whisky.country === 'Netherlands') {
			countryName = 'Nizozemí';
		}

		if (whisky.country === 'United Kingdom') {
			countryName = 'Velká Británie';
		}

		if (whisky.country === 'Iceland') {
			countryName = 'Island';
		}

		let selectedUsers = users.filter((user) => {
			let use = false;

			user.results.forEach((result) => result.items.forEach((item) => {
				if (item && typeof item === 'number' && item === whisky.id) {
					use = true;
				} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
					use = true;
				}
			}));

			return use;
		}).map((user) => {
			let newUser = _.cloneDeep(user);

			newUser.results = user.results.filter((result) => {
				let use = false;

				result.items.forEach((item) => {
					if (item && typeof item === 'number' && item === whisky.id) {
						use = true;
					} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
						use = true;
					}
				});

				return use;
			});

			return newUser;
		});

		return <div className={styles.root + (regionClass ? ` ${regionClass}` : '') + (isSingleMalt ? ` ${styles.isSingleMalt}` : '')}>
			{name(whisky)}

			<div className={styles.leftColumn}>
				<div className={styles.row}>
					{whisky.country ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Země</h4><p>{countryName}</p></div> : null}
					{whisky.region ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Oblast</h4><p>{regionName}</p></div> : null}
				</div>

				{whisky.distillery || whisky.bottler ? <div className={styles.row}>
					{whisky.distillery ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Palírna</h4><p>{whisky.distillery}</p></div> : null}
					{whisky.bottler ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Stáčírna</h4><p>{whisky.bottler}</p></div> : null}
				</div> : null}

				{whisky.age || whisky.vintage || whisky.bottled ? <div className={styles.row}>
					{whisky.age ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Stáří</h4><p>{`${whisky.age} let`}</p></div> : null}
					{whisky.vintage ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Ročník</h4><p>{parseDate(whisky.vintage)}</p></div> : null}
					{whisky.bottled ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Lahvováno</h4><p>{parseDate(whisky.bottled)}</p></div> : null}
				</div> : null}

				<div className={styles.row}>
					{whisky.strength ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Síla</h4><p>{`${NUMBER_FORMAT.format(whisky.strength)} %`}</p></div> : null}
					{whisky.size ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Objem</h4><p>{`${whisky.size * ML_IN_L} ml`}</p></div> : null}
					{whisky.caskNumber && whisky.caskType ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Sud</h4><p>{`${whisky.caskNumber} (${whisky.caskType})`}</p></div> : null}
					{!whisky.caskNumber && whisky.caskType ? <div className={styles.rowItem}><h4 className={styles.attributeHeading}>Sud</h4><p>{`${whisky.caskType}`}</p></div> : null}
				</div>

				<div className={styles.row}>
					<div className={styles.wideRowItem}>
						<ul className={styles.additionalProperties}>
							{whisky.isUncolored ? <li>Nebarvená</li> : null}
							{whisky.isNonChillfiltered ? <li>Nefiltrovaná za studena</li> : null}
							{whisky.isCaskStrength ? <li>V sudové síle</li> : null}
							{whisky.isSingleCask ? <li>Jednosudová</li> : null}
						</ul>
					</div>
				</div>

				<div className={styles.row}>
					{whisky.links && whisky.links.length ? <div className={styles.wideRowItem}><h4 className={styles.attributeHeading}>Odkazy</h4><p>{whisky.links.map((link, index) => <a key={index} href={link}>{link}</a>)}</p></div> : null}
				</div>
			</div>

			{whisky.image ? <figure className={styles.rightColumn} style={{
				backgroundImage: `url("${href('assets', whisky.image)}")`
			}}><img src={href('assets', whisky.image)} alt="" /></figure> : null}

			<section className={styles.fullColumn}>
				<h3 className={styles.votesHeading}>Umístění</h3>

				{selectedUsers && selectedUsers.length ? selectedUsers.map((user, userIndex) => <section key={userIndex} className={styles.result}>
					<header className={styles.resultHeader}>
						<h3 className={styles.resultHeading}>
							<span className={styles.resultHeadingLabel}>Uživatel</span>
							<a href={href('uzivatele', user.name)}>{user.name}</a>
						</h3>
					</header>

					<ol>
						{user.results.map((result, resultIndex) => {
							let category = _.find(categories, {name: result.categoryName});
							let points = 0;

							result.items.forEach((item, itemIndex) => {
								if (item && typeof item === 'number' && item === whisky.id) {
									points += category.points[itemIndex];
								} else if (item && typeof item === 'object' && typeof item.id === 'number' && item.id === whisky.id) {
									points += category.points[itemIndex];
								}
							});

							return <li key={resultIndex} className={styles.whisky}>
								<a href={href('kategorie', category.id)}>{result.categoryName}</a>
								{points ? <span className={styles.pointsCount}>{`${points} `}<span className={styles.pointsCountLabel}>{((value) => {
									if (value === 1) {
										return 'bod';
									}

									if (value === 2 || value === 3 || value === 4) {
										return 'body';
									}

									return 'bodů';
								})(points)}</span></span> : null}
							</li>;
						})}
					</ol>
				</section>) : null}
			</section>
		</div>;
	}
}

const ROUTE_STRIPPER = /^[#\/]|\s+$/g;

let appRoot = process.env.APP_ROOT.replace(ROUTE_STRIPPER, '');

export default function href(...levels) {
	return `${appRoot ? `/${appRoot}` : ''}/${levels.join('/')}`;
}

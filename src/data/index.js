/* eslint-disable quote-props, quotes, indent, no-magic-numbers */

import _ from 'lodash';
import ayu from 'ayu';
import cosineSimilarity from 'compute-cosine-similarity';
import euclideanDistance from 'compute-euclidean-distance';

import {users} from './users';
import {whiskies} from './whiskies';


export {users, whiskies};
export let categories = [{
	"id": 1,
	"group": 1,
	"name": "Whisky roku – absolutní vítěz",
	"points": [3],
	"hasWhiskies": true
}, {
	"id": 2,
	"group": 1,
	"name": "Skotsko single malt 9 a méně let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 3,
	"group": 1,
	"name": "Skotsko single malt 10 až 16 let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 4,
	"group": 1,
	"name": "Skotsko single malt 17 a více let",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 5,
	"group": 1,
	"name": "Skotsko single malt bez udání stáří",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 6,
	"group": 1,
	"name": "Blended whisky",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 7,
	"group": 1,
	"name": "Blended malt",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 8,
	"group": 1,
	"name": "Grain whisky",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 9,
	"group": 2,
	"name": "USA a Kanada",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 10,
	"group": 2,
	"name": "Irsko",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 11,
	"group": 2,
	"name": "Japonsko",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 12,
	"group": 2,
	"name": "Zbytek světa",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 13,
	"group": 3,
	"name": "Překvapení roku",
	"points": [3, 2, 1],
	"hasWhiskies": true
}, {
	"id": 14,
	"group": 4,
	"name": "Zklamání/propadák roku",
	"points": [],
	"hasWhiskies": true
}, {
	"id": 15,
	"group": 5,
	"name": "Poměr cena/výkon",
	"points": [],
	"hasWhiskies": true
}, {
	"id": 16,
	"group": 6,
	"name": "Kniha",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 17,
	"group": 6,
	"name": "Rum",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 18,
	"group": 6,
	"name": "Tuzemské pivo",
	"points": [],
	"hasWhiskies": false
}, {
	"id": 19,
	"group": 6,
	"name": "Zahraniční pivo",
	"points": [],
	"hasWhiskies": false
}];

export let votes = [];

let voteId = 0;

users.forEach((user) => {
	user.whiskiesVector = _.times(whiskies.length, _.constant(0));
	user.similarities = _.times(users.length, _.constant(0));
});

users.forEach((user) => user.results.forEach((result) => result.items.forEach((item, index) => {
	let id;

	if (item && typeof item === 'number') {
		id = +item;
	} else if (item && typeof item === 'object' && typeof item.id === 'number') {
		id = +item.id;
	} else {
		return;
	}

	let category = _.find(categories, {name: result.categoryName});

	if (!category) {
		return;
	}

	let vote = _.cloneDeep(_.find(whiskies, {id}));

	if (!vote) {
		return;
	}

	vote.whiskyId = vote.id;
	vote.id = ++voteId;
	vote.userId = user.id;
	vote.categoryName = category.name;
	vote.categoryGroup = category.group;
	vote.pointsCount = category.points && category.points[index] ? category.points[index] : 0;

	if (vote.pointsCount) {
		// user.whiskiesVector[vote.whiskyId - 1] = 1;
	}
	user.whiskiesVector[vote.whiskyId - 1] += vote.pointsCount;

	votes.push(vote);
})));

users.forEach((user1) => {
	users.forEach((user2, user2Index) => {
		user1.similarities[user2Index] = {
			userId: user2.id,
			userName: user2.name,
			cosineSimilarity: cosineSimilarity(user1.whiskiesVector, user2.whiskiesVector),
			euclideanDistance: euclideanDistance(user1.whiskiesVector, user2.whiskiesVector)
		};
	});

	user1.similarities.sort((similarity1, similarity2) => similarity2.cosineSimilarity - similarity1.cosineSimilarity);
});

global.whiskies = whiskies;
global.users = users;
global.cosineSimilarity = cosineSimilarity;

export let votesByCategory = new ayu.DataModel({
	whiskyIds: new ayu.Dimension({
		variable: 'whiskyId',
		scale: ayu.NOMINAL_SCALE
	}),
	voteIds: new ayu.Dimension({
		variable: 'id',
		scale: ayu.NOMINAL_SCALE
	}),
	categoryName: new ayu.Dimension({
		variable: 'categoryName',
		scale: ayu.NOMINAL_SCALE,
		group: true
	})
}, votes);

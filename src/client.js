import './internals/polyfills';

import $ from 'jquery';
import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import _ from 'lodash';

import App from './components/App';
import Stream from './libs/Stream';
import router from './streams/router';
import BrowserRouter from './libs/BrowserRouter';
import I18n from './libs/I18n';
import config from './config';
import constants from './internals/constants';


global.React = React;
global.$ = $;
global.Stream = Stream;
global.moment = moment;
global._ = _;

const G_KEY_CODE = 71;
const EN = constants.EN;

$('html').removeClass('noJavascript');

// grid toggle
$(document).on('keydown', (event) => {
	let tagName = event.target.tagName.toLowerCase();

	if (event.keyCode === G_KEY_CODE && event.target && tagName !== 'textarea' && tagName !== 'input') {
		$('body').toggleClass('hasGrid');
	}
});

// init i18n
moment.locale('cs');

let i18n = new I18n();

// default i18n settings, must be applied before creating router!
i18n.use({
	strings: config.i18nStrings,
	locale: EN,
	currency: '$'
});

// init router
let browserRouter = new BrowserRouter(router);

browserRouter.start();

// render app
let rootNode = document.getElementById('app');

if (rootNode) {
	ReactDOM.render(<App />, rootNode);
}

console.log('Oj!');

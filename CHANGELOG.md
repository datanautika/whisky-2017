Whisky of the year 2017
=======================


## v2.0.1

- Fix data
- Fix bugs


## v2.0.0

- Update to the year 2017
- Add menu
- Add similarity between users


## v1.0.8

- Fix data


## v1.0.7

- Fix data


## v1.0.6

- Fix data
- Add various tweaks


## v1.0.5

- Fix data


## v1.0.4

- Increase awarded points in the Whisky of the year category
- Tweak whisky view
- Fix bug with data point IDs


## v1.0.3

- Improve performance
- Fix category view


## v1.0.2

- Show all categories
- Tweak some styles


## v1.0.1

- Update data & photos
- Add links connecting users, categories and whiskies
- Unify design of votes lists
- Fix breadcrumbs
- Fix scroll position restoration
- Add version info to the footer
- Add various tweaks


## v1.0.0

- Initial version
